<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class Videos extends Model
{
   	public $table 	   = 'videos';
    public $timestamps = true;
    protected $fillable = ['id', 'title', 'video'];

    public function display() 
    { 
       return DB::table('videos')
						->orderby('created_at', 'desc')
						->get();
    }

}