<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Http\Controllers\UploadController;

class Uploads extends Model
{
   	public $table 	   = 'uploads';
    public $timestamps = false;
    protected $fillable = ['id', 'file_name', 'file_type', 'file_mime', 'file_size', 'pushtime', 'parent_id', 'file_position'];

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }

    public function getPhoto($ID)
    {
        $query = self::find($ID);
        
        if($query != null) {
                $uploader = new UploadController();
        return  (object)array('photo'=>$uploader->retrivePathWithDate($query->pushtime).$query->file_name,'query'=>(object)$query);
        } else {
            return (object)array(
                'photo'=>'/images/icon/default_lg.png', 
                'query'=>(object)array('id'=>0,'file_name'=>'','file_size'=>'')
                );
        }
        
    }

}
