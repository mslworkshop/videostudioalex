<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use DB;
use App\Uploads;
use App\Http\Controllers\UploadController;

class Gallery extends Model
{
    public $table 	   = 'gallery';
    public $timestamps = true;
    protected $fillable = ['id','title', 'description', 'video_url', 'file_id'];

    public function dataRowbyPage()
    {
    	if(isset($_GET['page'])) { $page = $_GET['page']; } else { $page = 1; }
    	$query=null;
		
		$query = DB::table('gallery')
					->orderby('created_at', 'desc')
					->paginate(20, ['*'], 'page', $page);
		return $query;
    }

    public function pictureByID($GalleryID)
    {
    	$sql = DB::table('uploads')
                ->select(['id','file_name','pushtime'])
                ->where(['parent_id'=>$GalleryID, 'file_type' => 2])
                ->orderby('file_position', 'asc')
                ->get();
        $default = (object) array('id'=>0,'file_name'=>'0509389f41c8_be749717fa3df9f3a1327b908eb856cd.png','pushtime'=>1508939513);

        if(count($sql) > 1){
            foreach($sql as $showImage) {
                if(UploadController::checkFile($showImage->id) != (null or false)) {
                    return $showImage;
                } else {   
                    if(count($sql) < 1) { 
                        continue;
                    } else {
                        return $default;
                    }
                }
            }
        } else {
            return $default;
        }
    
    }

    public static function GalleryFilesByID($GalleryID)
    {   
    	//$count = Uploads::where(['gallery_id' => $GalleryID])->count();
        	return  (object) self::find($GalleryID)->uploads()->orderby('file_position', 'asc')->get();
    }


    public static function displayGallery()
    {   
    	$store = null;

        foreach (self::all()->sortByDesc('created_at') as $b3 => $b4) {
            $store[] = (object)  array(
                'data'=>$b4,
                'files'=>self::find($b4->id)->uploads()->orderBy('file_position', 'asc')->get()
            );
        }
        return $store;
    }

    public function uploads()
    {
    	
     return $this->hasMany('App\Uploads','parent_id','id');
    }


    public function scopeRowbyID($ID)
    {
    	return self::find($ID);
    }

    public function scopeRowAll()
    {
    	return self::all();
    }
}
