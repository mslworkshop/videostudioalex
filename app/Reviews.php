<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Reviews extends Model
{
    public $table 	   = 'reviews';
    protected $fillable = ['id','title','file_id', 'description'];

    public function pictureByID($ID)
    {
    return   $sql = DB::table('uploads')
    	    		->select(['id','file_name','pushtime'])
    				->where(['parent_id' => $ID, 'file_type' => 3])
    				->first();
    }


    public function display()
    {
        return self::all();
    }
}
