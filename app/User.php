<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'ctrlCode', 'last_user_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function isAdmin($userID)
    {
        if(Auth::check())
        {
            $queryCheck = User::where('id', $userID)->pluck('ctrlCode');
            $access = $queryCheck[0];
            
                if($access == 1) return true; 

                return false; 
        }
        return false;
    }

}
