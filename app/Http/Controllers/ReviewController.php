<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\UploadController;
use DB;
use User;
use App\Reviews;
use App\Uploads;

class ReviewController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function Home()
    {
    	  	if(isset($_GET['page'])) { $page = $_GET['page']; } else { $page = 1; }
			$Query = DB::table('reviews')
						->orderby('created_at', 'desc')
						->paginate(8, ['*'], 'page', $page);
			$Query->withPath('/reviews');

    	return view('review.index')->with('query',$Query);
    }

    public function ViewbyID($ID)
    {
    	$Query = Reviews::find($ID);

    return view('review.view')->with('data',$Query);
    }

    public function addNewPhoto(Request $request)
    {
    	$uploader = new UploadController();
        $uploadReturn = $uploader->upload($request);
            return $uploadReturn;
    }

    public function deleteReview($ID)
    {
        $Query = Reviews::find($ID);
        if($Query != null)
        {
            if($Query->file_id != NULL)
            {
            $File = UploadController::checkFile($Query->file_id);
                        unlink($File->isFile);
            Uploads::find($Query->file_id)->delete();
            }
            $Query->delete();
            return redirect('/reviews')->with('status_report',"Review was... deleted!"); 
        }
        return redirect('/reviews')->with('status_report',"Review was... Not found!");
    }

    public function ReviewManager(Request $request)
    {
    	
    	$_error = false;
    	$_function = NULL;
    	$debug = false;
   		$Return = array();

   		# POST Definitions
        $package = $request->input('package');
   		$revID = $request->input('bankIDReview');
   		$_exec = $request->input('exec');
    	
    	if($revID==null || empty($revID)) 
    	{ 
    	$_error=true;
    		$Return = array(
				'status'=>'danger',
				'report'=>'Critical error',
				'exec'=>"GalleryManager:blankID($revID)::L".__LINE__
			);
    	}

   		$validator = Validator::make($request->all(), [
	            'title' 	  => 'required|min:3|max:250',
	            'description' => 'required|min:1|max:2500'
	    ]);

	    if($validator->fails() && !$_error) {
	    $_error=true;
	      	$Return = array(
	      		'ID' => $revID,
				'status'=>'danger',
				'report'=>'Critical error',
				'exec'=>"ReviewController:Validator()::L".__LINE__
			);
		}

        if(!$_error){
		$forceEdit = Reviews::find($revID);
		if(!empty($forceEdit->id))
		{
			if(($forceEdit->id == $revID))
		 	{
		 		$_exec = 'edit';
			}
		}
    	switch ($_exec) {
    		case 'create':
    			$_resourceOuput = Reviews::Create([
                    // 'id' => $revID,
    				'file_id'	=> $request->input('file_id'),
    				'title'	=>	$request->input('title'),
    				'description' => $request->input('description'),
    				]);
                $uploadedFile = Uploads::find($request->input('file_id'));
                $uploadedFile->parent_id = $_resourceOuput->id;
                $uploadedFile->save();
    			$_function = "ReviewManager->Create($revID)::L".__LINE__;
    			break;

    		case 'edit':
    			$postData = array('title','description', 'file_id');
    				foreach ($postData as $null => $data) {
    					$value = $request->input($data);
		    		   		if(!empty($value) or $value != null){
			    				$forceEdit->$data = $value; 
			    			}	
    				}
    			$_resourceOuput = $forceEdit->save();
    			$_function = "ReviewManager->Edit($revID)::L".__LINE__;
    			break;
    	}

    	if($_resourceOuput)
    	{
    		$Return = array('status' => 'success', 'report' => 'Operation successful' );
    	} else {
    		$Return = array('status' => 'danger', 'report' => 'Query failed');
    	}
    }
        if(!$_error) {
    	$Return2 = array('debug'=>$debug, 'ID' => $revID,'exec' => $_function)+$Return;
    } else {
        $Return2 = array('debug'=>$debug, 'ID' => $revID)+$Return;
    }

    	return json_encode((object)$Return2);
    }
}
