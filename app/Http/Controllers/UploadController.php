<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMimeFailedException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use App\Uploads;
use App\Reviews;
use App\Gallery;

class UploadController extends Controller
{
    public static $dateFormat = "dmYW";
    public static $UploadFolder = "/upload_files/";
    public static $allowed_images = array(
            'image/gif',
            'image/png',
            'image/bmp',
            'image/jpeg');
    public static $allowed_videos = array(
            'video/mp4',
            'video/avi',
            'video/msvideo',
            'video/x-msvideo',
            'video/quicktime');

    public function txData($request)
    {
        $package = $request->input('sysPack');
        switch ($package) {
            case 'sys.review' : $extCode = 3;   break;
            case 'sys.gallery' : $extCode = 2; break;
            default:   $extCode = $package;   break;
        }
        
        return array($request->input('bankID'), $extCode);
    }

    public function setFileState($ID,$Type,$value=null)
    {
        switch ($Type) {
            case 2: $query = Gallery::find($ID);    break;
            case 3: $query = Reviews::find($ID);    break;
        }
        $query->file_id=$value;
        $query->save();
    }

     public function deletePhotoByID($FileID)
     {
        $query = Uploads::find($FileID);
        
            self::setFileState($query->parent_id,$query->file_type,0);

            $out = self::deleteFile($FileID);
        return response()->json($out);
     }

    /**
     * Handles the file upload
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     */
    public function upload(Request $request) {
        // create the file receiver
        

        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));

        // check if the upload is success
        if ($receiver->isUploaded()) {

            // receive the file
            $save = $receiver->receive();

            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                // save the file and return any response you need
                return $this->saveFile($save->getFile(), self::txData($request));
            } else {
                // we are in chunk mode, lets send the current progress

                /** @var AbstractHandler $handler */
                $handler = $save->handler();

                return response()->json([
                    "done" => $handler->getPercentageDone(),
                ]);
            }
        } else {
            throw new UploadMissingFileException();
        }
    }



    /**
     * Saves the file
     *
     * @param UploadedFile $file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function saveFile(UploadedFile $file, $ARGv=null,$filePosition=null)
    {
        $rowName = $this->createFilename($file);
        $splitName  = explode('_',$rowName);
        $fileID = $splitName[0];
        $fileName = $rowName;
        $fullMine = $file->getMimeType();
        $fileSize = $file->getSize();
        $pushtime = time();

        // Check mime type is allowed
        if(!$this->checkFileMIME($fullMine)) { throw new UploadMimeFailedException; }

        // Group files by mime type
        $mime = str_replace('/', '-', $fullMine);
        // Group files by the date (week
        $finalPath = $this->retriveFullPath($pushtime);

        // Build the file path

        //$filePath = "upload/{$mime}/{$dateFolder}/";
        //$filePath = "upload_files/{$dateFolder}";
        //$finalPath = storage_path("app/".$filePath);
        //$finalPath = public_path($filePath);

        // move the file name
        $file->move($finalPath, $fileName);

        //Insert a row of data for uploaded file
        Uploads::create([ 
            'id' => $fileID,
            'file_name' => $fileName,
            'file_mime' => $fullMine,
            'file_type' => $ARGv[1], //self::checkFileType($fullMine),
            'file_size' => $fileSize,
            'file_position'  => $filePosition,
            'pushtime'  => $pushtime,
            'parent_id' => $ARGv[0]
        ]);

        /*
         * Update first time the picon of the record
         */
        // var_dump($ARGv);exit;
        $query = false;
        if($ARGv[1] == 2){
          $query = Gallery::find($ARGv[0]); 
        } elseif ($ARGv[1] ==  3) {
          $query = Reviews::find($ARGv[0]);
        }
        $upFile = Uploads::find($fileID);
        $upFile->parent_id = $ARGv[0]; 
        $upFile->file_type = $ARGv[1];
        $upFile->save();

        // really first time
        /*if($query && !empty($query->file_id)) {
            var_dump(123);
            $query->file_id = $fileID;
            $query->save();   
        }*/
        if($query != null) {
            $upFile = Uploads::find($fileID);
            $upFile->parent_id = $ARGv[0]; 
            $upFile->file_type = $ARGv[1];
            $upFile->save();

            $query->file_id = $fileID;
            $query->save();
            
        }

        return response()->json([
            'id' => $fileID,
            'path' => self::$UploadFolder.date(self::$dateFormat),
            'name' => $fileName,
            'mime_type' => $mime,
            'parent_id' => $ARGv[0],
            'parent_type' => $ARGv[1],
            'file_position' => $filePosition
        ]);
    }

    /**
     * Create unique filename for uploaded file
     * @param UploadedFile $file
     * @return string
     */
    protected function createFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $real_name = $file->getClientOriginalName();
        //$filename = str_replace(".".$extension, "", $real_name); // Filename without extension
        $filename = substr((rand(999,9999)*0x08011012)^0x9229*rand(2,22),2,8);
        // Add timestamp hash to name of the file
        $filename .= "_" . md5(time()) . "." . $extension;

        return $filename;
    }

/**
 * Handles the file upload
 *
 * @param Request $request
 *
 * @param Int $fileIndex
 *
 * @return \Illuminate\Http\JsonResponse
 * 
 * @throws UploadMissingFileException
 */
public function MultiUpload(Request $request) {
    // Response for the files - completed and uncompleted
    $files = [];

    // Get array of files from request
    $files = $request->file('files');
    
    if (!is_array($files)) {
        throw new UploadMissingFileException();
    }

    $fileCount=1;
    // Loop sent files
    foreach ($files as $file) {
        // Instead of passing the index name, pass the UploadFile object from the $files array we are looping
        // Exception is thrown if file upload is invalid (size limit, etc)
        
        // Create the file receiver via dynamic handler
        $receiver = new FileReceiver($file, $request, HandlerFactory::classFromRequest($request));
        // or via static handler usage
        //$receiver = new FileReceiver($file, $request, ContentRangeUploadHandler::class);
        
        if ($receiver->isUploaded()) {
            // receive the file
            $save = $receiver->receive();
    
            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                // save the file and return any response you need
                $files[] = $this->saveFile($save->getFile(),self::txData($request),$fileCount);
            } else {
                // we are in chunk mode, lets send the current progress
    
                /** @var ContentRangeUploadHandler $handler */
                $handler = $save->handler();
                
                // Add the completed file
                $files[] = [
                    "start" => $handler->getBytesStart(),
                    "end" => $handler->getBytesEnd(),
                    "total" => $handler->getBytesTotal(),
                    "finished" => false
                ];
            }
        }
        $fileCount++;
    }
    //return array($files);
    return response()->json($files);
}
    
    private function checkFileType($input)
    {
        if(in_array($input, self::$allowed_images)) return "image";

        if(in_array($input, self::$allowed_videos)) return "video";
    }

    private function checkFileMIME($input)
    {

        if(in_array($input, self::$allowed_images)) return true;

        if(in_array($input, self::$allowed_videos)) return true;

            return false;
    }
    public static function retriveFullPath($date) {  
        return public_path().self::retrivePathWithDate($date); 
    }

    public static function retrivePathWithDate($date) {
        return self::$UploadFolder.date(self::$dateFormat, $date).'/';
    }

    public static function deleteFile($ID)
    {
        $File = self::checkFile($ID); 

        if($File != false) {
                        $deleting = unlink($File->isFile);
                            if($deleting) { 
                                Uploads::find($ID)->delete(); 
                return true; // back()->with('status_report', "Deleting OK.");
            }
            return false;
        } //else { return  "[ 404 ] :: File not found"; }
    }

    public static function checkFile($ID)
    {
        if(!empty($ID)){
        $query = Uploads::select(['id', 'file_name', 'pushtime'])->where('id',$ID)->first();
        
            if($query != NULL){
                    if($query->id == $ID) { 
                        (object) $query->full_path = self::retrivePathWithDate($query->pushtime).$query->file_name;
                        (object) $query->file_path = self::$UploadFolder;
                        (object) $query->filedate_path = self::$UploadFolder.date(self::$dateFormat,$query->pushtime).'/';
                        (object) $query->isFile = self::retriveFullPath($query->pushtime).$query->file_name;
                            if(!is_file($query->isFile)){
                                    return false; //File not exists
                            }
                        return $query; 
                    }
            } else {
                return -2; //Record ID not exists
            }
        }
        return -1; //Empty input
    }

    public function viewFile($ID)
    {
        $verifyFile = $this->checkFILE($ID);
            if(!$verifyFile) { throw new UploadMissingFileException(); }
        //$verifyFile->filedate_path.$verifyFile->file_name;
    echo "<img src=\"".$verifyFile->filedate_path.$verifyFile->file_name."\" >";
    }
}