<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Videos;
use DB;

class VideoController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Get Youtube video ID from URL
	 *
	 * @param string $url
	 * @return mixed Youtube video ID or FALSE if not found
	 */
	function getYoutubeIdFromUrl($url) {
        return $url;
	    $parts = parse_url($url);
	    if(isset($parts['query'])){
	        parse_str($parts['query'], $qs);
	        if(isset($qs['v'])){
	            return $qs['v'];
	        }else if(isset($qs['vi'])){
	            return $qs['vi'];
	        }
            return false;
	    } else { return false; }

	    if(isset($parts['path'])){
	        $path = explode('/', trim($parts['path'], '/'));
	        return $path[count($path)-1];
	    }
	    return false;
	}

	public function Home()
	{
		if(isset($_GET['page'])) { $page = $_GET['page']; } else { $page = 1; }
			$data = DB::table('videos')
						->orderby('created_at', 'desc')
						->paginate(6, ['*'], 'page', $page);
			$data->withPath('/video');

		return view('video.index')->with('Query',$data);
	}

    public function VideoManager(Request $request)
    {
    	$bank = $request->input('bankID');
    	$error = false; $debug = false; $id=0;
    	$func = null; $responce=array(); $state=null;

   		$validator = Validator::make($request->all(), [
	            'title' 	=> 'required|min:3|max:50',
	            'video' 	=> 'required|min:3|max:100'
	    ]);

	    if($validator->fails()) {
	    $error=true;
	      	$responce = array(
					'status'=>'danger',
					'report'=>'Critical error! Checkout form data.',
			);
					$func = 'VideoController->Validator()::L'.__LINE__;
		}
			
			$postTitle = $request->input('title');
    		$postVideo = $request->input('video');
            $postVideo = self::getYoutubeIdFromUrl($postVideo);

    	if($postVideo == false)
    	{
    		$error=true;
    		$responce = array(
    			'status'=>'danger',
    			'report'=>'Video URL is not valid.');
    		$func = 'VideoController->VideoValidator($postVideo)::L'.__LINE__;
    	}

        if(!$error){
    	if(!empty($bank) && $bank != 'add') /* Lets edits the bank record */
    	{
			   	
    		$VideoQuery = Videos::find($bank);
    		if($VideoQuery) {
    			$VideoQuery->title = $postTitle;
    			$VideoQuery->video = $postVideo;
    			$state = $VideoQuery->save();
    			$func = 'VideoController->Edit()::L'.__LINE__;
    			$id = $bank;
    		} else {
    			/* Fake request, ID not exists*/
    		}
    	} elseif($bank == "add") { #or create a new one
    		$state = Videos::create(['title'=>$postTitle, 'video'=>$postVideo]);
    		$func = 'VideoController->Create()::L'.__LINE__; 
    		$id = Videos::count();
    	}
        }

    	if($state)
    	{
    		$responce = array('status' => 'success', 'report' => 'Operation successful');
    	} else {
    		if(!$error) $responce = array('status' => 'danger', 'report' => 'Query failed');
    	}

    	$objReturn = array(
				'debug'=>$debug,
				'exec' => $func, 
				'ID' =>$id)+$responce;
    		
    	
			return json_encode((object)$objReturn);
    }

    public function deleteVideo($id) 
    {
    	$checkVideo = Videos::findOrfail($id);
    		if($checkVideo)
    		{
				$checkVideo->delete();
				return redirect('/video')->with('status_report', "Action complete...Video Deleted");
    		} else {
    			return redirect('/video')->with('status_report', "Not found or empty! #404");
    		}
    }
}
