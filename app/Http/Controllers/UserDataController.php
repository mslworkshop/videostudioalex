<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\UserHistory;
use App\User;
use Auth;
use DB;

class UserDataController extends Controller
{
	protected $redirectTo = '/home';

	public function __construct()
	{
		$actUser = new User;
		$isAdmin = $actUser->isAdmin(Auth::id());
		if($isAdmin)
		{
			return redirect('/login');
		}
	}

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:20',
            'email' => 'required|string|email|max:20',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

	public function saveUserData(Request $request)
	{
		$userQuery = User::find(Auth::id());

		if(!empty($request->input('name'))) { $userQuery->name = $request->input('name'); }
		if(!empty($request->input('email'))) { $userQuery->email = $request->input('email'); }
		if(!empty($request->input('password'))) { $userQuery->password = bcrypt($request->input('password')); }

	if($userQuery->save()) return back()->with('status_report', "UserData changed!");

				return back()->with('status_report', "Something goes wrong...");
	}

    public function saveSettingsPanel(Request $request)
    {

    	return back()->with('status_report', "Something goes wrong...");
    }

}