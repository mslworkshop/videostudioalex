<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\UploadController;
use DB;
use User;
use App\Gallery;
use App\Uploads;

class GalleryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    	public function view_Gallery()
    	{
    		return view('gallery.index')->with('query',Gallery::displayGallery());
    	}

    	public function view_addGallery()
    	{
    		return view('gallery.add');
    	}

    	public function view_editGallery()
    	{
    		return view('gallery.edit');
    	}

    	public function view_byID($ID)
    	{
    		if(!empty($ID)){
    			$data = Gallery::find($ID);
    			return view('gallery.view')->with('data',$data);
    		} else {
    			return redirect('/gallery')->with('status_report', "[ $ID ] Not found or empty! #404");
    		}
    	}

/*
 * @vendor: API commnycation
 * @method: POST
 */
    	public function addNewPhotos(Request $request)
    	{
    		$uploader = new UploadController();
    		$uploadReturn = $uploader->MultiUpload($request);
    		return $uploadReturn;
    	}

    	public function GalleryManager(Request $request)
    	{
    		$postData = array('title', 'description', 'video_url');
    		$hasError = false;
    		$objReturn = array();
    		$GalleryID = $request->input('bankIDGallery');
    		$ExecType  = $request->input('exec');
    		$ExecFunction = NULL;
    		$debug = false;

    		$postTitle = $request->input('title');
    		$postDesc = $request->input('description');
    		$postInfo = $request->input('video_url');

    		if(empty($GalleryID) || $GalleryID == null) 
    		{ 
    		$hasError=true;
    		$objReturn = array(
    			'ID' => $GalleryID,
    			'debug' => $debug,
				'status'=>'danger',
				'report'=>'Critical error',
				'exec'=>"GalleryManager:blankID($GalleryID)::L".__LINE__
				);
    		}

	        $validator = Validator::make($request->all(), [
	            'title' 	  => 'required|min:3|max:250',
	            'description' => 'required|min:5|max:2500',
	        ]);

	        if($validator->fails() && !$hasError) {
	        $hasError=true;
	      	$objReturn = array(
	      		'ID' => $GalleryID,
	      		'debug' => $debug,
				'status'=>'danger',
				'report'=>'Critical error',
				'exec'=>"GalleryManager:Validator()::L".__LINE__
				);
		    }


		$forceEdit = Gallery::find($GalleryID);
		if(!empty($forceEdit->id)){
		 if(($forceEdit->id == $GalleryID))
		 {
		 	$ExecType = 'edit';
		 }
		}

		 if(!$hasError){
    		switch ($ExecType) {
    			case 'create':
	 	    		$resOUT = Gallery::create([
	    			'id' 		  => $GalleryID,
	    			'title' 	  => $postTitle,
	    			'description' => $postDesc,
	    			'video_url' => $postInfo
	    			]);
	    	$ExecFunction = "addGalleryElement::L".__LINE__;
    		break;

    			case 'edit':
		  		    	foreach ($postData as $null => $data)
		    		   	{
			   		   		$value = $request->input($data);
		    		   		if(strlen($value) >= null){
			    				$forceEdit->$data = $value; 
			    			}
			    		}
			    		$resOUT = $forceEdit->save();
			$ExecFunction = "editGalleryElement::L".__LINE__;
    		break;

    			default : 
    				$ExecFunction = "Unknown Function($ExecType)::L".__LINE__;
    		break; 
    	}
    	
    					if($resOUT) {
	    					$objReturn = array(
	    						'status'=>"success",
	    						'report'=>'Operation successful'
	    						);
	    				} else {
	    					$objReturn = array(
	    						'status'=>"danger",
	    						'report'=>"Query failed"
	    						);
	    				}

			$objReturn = array(
				'debug'=>$debug,
				'exec' => $ExecFunction, 
				'ID' => $GalleryID)+$objReturn;
    		
    	}
			return json_encode((object)$objReturn);
    	
    	}

    	public function deleteGallery($GalleryID)
    	{
    		$Query = Gallery::find($GalleryID);
    		if($Query)
    			{	
                    $out = $Query->uploads()->get();
                    foreach ($out as $key) {    
                        $ID = $key->id;
                        $File = UploadController::checkFile($ID);
                     if($File){
                           unlink($File->isFile);
                     }
                     Uploads::find($ID)->delete();
                    }

    				$Query->delete();
    					return redirect('/gallery')->with('status_report', "Gallery ($GalleryID) was deleted.");
    			}
    		return redirect('/gallery')->with('status_report',"Gallery was... Not found!");
    	}

}
