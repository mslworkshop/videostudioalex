<?php
if($_POST) {
    /*Inlcude API configs*/
    //include("api-config.php");

    $jsonfile = file_get_contents("results.json");
    $data = json_decode($jsonfile, true);

    if (isset($data['mailchimp_api']) && isset($data['mailchimp_listid'])) {

        $api_key = $data['mailchimp_api'];
        $list_id = $data['mailchimp_listid'];

        // Getting params from request
        $fname = isset($_POST['fname']) ? $_POST['fname'] : '';
        $lname = isset($_POST['lname']) ? $_POST['lname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';

        /**
         *  Possible Values for Status:
         *  subscribed, unsubscribed, cleaned, pending, transactional
         **/
        $status = 'subscribed';
        if($email) {
            $data = array(
                'apikey'        => $api_key,
                'email_address' => $email,
                'status'     => $status,
                'merge_fields'  => array(
                    'FNAME' => $fname,
                    'LNAME' => $lname
                )
            );

            // URL to request
            $API_URL =   'https://' . substr($api_key,strpos($api_key,'-') + 1 ) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($data['email_address']));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $API_URL);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$api_key )));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data) );
            $result = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($result);

            if( $response->status == 400 ){
                echo json_encode(array('status' => 400, 'msg' => 'Грешка!'));
                /*
                foreach( $response->errors as $error ) {
                    //echo 'Error: ' . $error->message . '<br>';
                }*/
            } elseif( $response->status == 'subscribed' ){
                echo json_encode(array('status' => 200, 'msg' => 'Благодарим ви! Абонирахте се успешно!'));
                // echo 'Thank you. You have already subscribed.';
            }elseif( $response->status == 'pending' ){
                echo json_encode(array('status' => 200, 'msg' => 'Вашето абониране очаква одобрение. Моля, проверете пощата си!'));
                // echo 'You subscription is Pending. Please check your email.';
            }
        }
    } else {
        echo json_encode(array('status' => 400, 'msg' => 'Грешка! Не сте въвели API key и List Id'));
    }
}

?>