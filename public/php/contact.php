<?php
if($_POST) {
    /*Inlcude API configs*/
    //include("api-config.php");

    //Success or Error Messages
    $MSG_REQUIRED_FIELDS = "Please, fill all fields marked with *";
    $msg_invalid_email_address = "Please enter a valid email address.";
    $msg_php_email_sent = "Thank you for contacting us. We will carefully review your message and contact you back.";
    $msg_php_email_not_sent = "Ooops! The message was not sent!";

    $jsonfile = file_get_contents("results.json");
    $data = json_decode($jsonfile, true);
    $purchase_email = $data['mail'];
    $tz_from_email = isset($data['mail_from']) ? $data['mail_from'] : '';
    $email_subject = $data['mail_subject'];
    // var_dump(0);

    $name = $_POST['name'];
    $email = $_POST['email'];
    $txt = $_POST['comment'];
    $phone = $_POST['phone'];
    $venue = $_POST['venue'];
    $date = $_POST['eventDate'];
    $option = $_POST['option'];
    $message =  "Name: " . $name . "\r\nMail: " . $email . "\r\n";
    if(empty($name) || empty($email) || empty($txt)){
        echo json_encode(array('status' => 400, 'msg' => $MSG_REQUIRED_FIELDS));
        die;
    }
    if (isset($phone) && !empty($phone)) {
        $message .= 'Phone: ' . $phone . "\r\n";
    }
    if (isset($date) && !empty($date)) {
        $message .= 'Date: ' . $date . "\r\n";
    }
    if (isset($venue) && !empty($venue)) {
        $message .= 'Venue: ' . $venue . "\r\n";
    }
    if (isset($option) && !empty($option)) {
        $option .= 'Option: ' . $option . "\r\n";
    }
    $message .= 'Message:' ."\r\n" . $txt . "\r\n";

    $headers = "MIME-Version: 1.0\r\n";
    // $headers .= "Content-Type: text/html; charset=utf-8\r\n";
    $headers .= 'From: '. $email. "\r\n";
    $headers .= 'Reply-To: '. $email. "\r\n";
    $headers .="Content-Type: text/plain; charset=\"utf-8\"\r\n";
    $mail = mail($purchase_email, $email_subject, $message, $headers);

    if (!$mail) {
        //header('Content-Type: application/json');
        echo json_encode(array('status' => 400, 'msg' => $msg_php_email_not_sent));
        die;
    }

    header('Content-Type: application/json');
    echo json_encode(array('status' => 200, 'msg' => $msg_php_email_sent, 'mail' => $mail));
}

?>