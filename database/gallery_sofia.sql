-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 09, 2017 at 02:51 PM
-- Server version: 5.7.18
-- PHP Version: 7.0.21

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gallery_sofia`
--

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `text` text COLLATE utf8mb4_unicode_ci,
  `type` int(11) NOT NULL DEFAULT '0',
  `file_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'off',
  `priority` int(11) NOT NULL DEFAULT '1',
  `pushtime` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `gallery`
--

TRUNCATE TABLE `gallery`;
--
-- Dumping data for table `gallery`
--

INSERT IGNORE INTO `gallery` (`id`, `title`, `description`, `text`, `type`, `file_id`, `status`, `priority`, `pushtime`, `created_at`, `updated_at`) VALUES
(1, 'Искаш да си герой в приказка?', '\"...остави се в нашите ръце\"', '\"We are a small boutique with a cute little shop on the high street and we are now selling some of our gorgeous stock online.\"', 1, '522226bed708', 'on', 3, 1504883020, NULL, '2017-09-09 11:24:02'),
(2, 'Искаш да си герой в приказка?', '\"...остави се в нашите ръце\"', '\"We are a small boutique with a cute little shop on the high street and we are now selling some of our gorgeous stock online.\"', 1, '633b870602ef', 'on', 2, 1504883710, NULL, '2017-09-09 11:23:12'),
(3, 'Искаш да си герой в приказка?', '\"...остави се в нашите ръце\"', '\"We are a small boutique with a cute little shop on the high street and we are now selling some of our gorgeous stock online.\"', 1, 'a8145dd0c0d5', 'on', 1, 1504892098, NULL, '2017-09-09 11:22:41'),
(4, 'ВИНТИДЖ', '\"Lorem Ipsum has been the industry\'s standard dummy text ever since, when an unknown printer took a galley of type and scrambled it to make a type specimen book dummy text.\"', '', 3, 'ee0bf4281db0', 'on', 1, 1504953960, NULL, '2017-09-09 11:20:36'),
(5, 'ВИНТИДЖ', '\"Lorem Ipsum has been the industry\'s standard dummy text ever since, when an unknown printer took a galley of type and scrambled it to make a type specimen book dummy text.\"', '', 3, '96d6447de180', 'on', 2, 1504954035, NULL, '2017-09-09 11:21:01'),
(6, 'ВИНТИДЖ', '\"Lorem Ipsum has been the industry\'s standard dummy text ever since, when an unknown printer took a galley of type and scrambled it to make a type specimen book dummy text.\"', '', 3, '20325e1ee24d', 'on', 3, 1504954262, NULL, '2017-09-09 11:21:21'),
(7, 'ТОРТАТА', '\"We can deliver flowers within 25 miles of our shop between the hours of 9.30am and 5.30pm Monday to Saturday\"', '', 2, 'fe9540408b3c', 'on', 1, 1504955833, NULL, NULL),
(8, 'УКРАСАТА', '\"We can deliver flowers within 25 miles of our shop between the hours of 9.30am and 5.30pm Monday to Saturday\"', '', 2, 'd45dd911cb4b', 'on', 2, 1504955865, NULL, NULL),
(9, 'ЦВЕТЯТА', '\"We can deliver flowers within 25 miles of our shop between the hours of 9.30am and 5.30pm Monday to Saturday\"', '', 2, 'a382585dd3a4', 'on', 3, 1504955890, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Dumping data for table `migrations`
--

INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
(25, '2014_10_12_000000_create_users_table', 1),
(26, '2014_10_12_100000_create_password_resets_table', 1),
(27, '2017_08_31_051204_create_upload_table', 1),
(28, '2017_08_31_051211_create_gallery_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `password_resets`
--

TRUNCATE TABLE `password_resets`;
-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `file_id` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_mime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pushtime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `uploads`
--

TRUNCATE TABLE `uploads`;
--
-- Dumping data for table `uploads`
--

INSERT IGNORE INTO `uploads` (`file_id`, `file_name`, `file_type`, `file_mime`, `pushtime`) VALUES
('20325e1ee24d', '20325e1ee24d_e58e4f5d74b46c37ebeff1b6ac70ed4b.jpg', 'image', 'image/jpeg', 1504949929),
('522226bed708', '522226bed708_181b146c731d4dfe271ead122ab19c09.jpg', 'image', 'image/jpeg', 1504882940),
('633b870602ef', '633b870602ef_181b146c731d4dfe271ead122ab19c09.jpg', 'image', 'image/jpeg', 1504882940),
('96d6447de180', '96d6447de180_cd99f4a6b785489bf310e441a95acf09.jpg', 'image', 'image/jpeg', 1504949928),
('a382585dd3a4', 'a382585dd3a4_46e10e06936089640c91db23cb31ddcb.png', 'image', 'image/png', 1504950545),
('a8145dd0c0d5', 'a8145dd0c0d5_181b146c731d4dfe271ead122ab19c09.jpg', 'image', 'image/jpeg', 1504882940),
('d45dd911cb4b', 'd45dd911cb4b_46e10e06936089640c91db23cb31ddcb.png', 'image', 'image/png', 1504950545),
('ee0bf4281db0', 'ee0bf4281db0_93f8b42b11db8bea04b5c4c7d429fdff.jpg', 'image', 'image/jpeg', 1504949928),
('fe9540408b3c', 'fe9540408b3c_46e10e06936089640c91db23cb31ddcb.png', 'image', 'image/png', 1504950545);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ctrlCode` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT IGNORE INTO `users` (`id`, `name`, `email`, `password`, `ctrlCode`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@unix-ltd.net', '$2y$10$YGu2a6G0w/LY3X.FpD4hPOciCN785QrMeOc3uA0oAowP636Kpgdp.', 1, NULL, '2017-09-08 15:01:37', '2017-09-08 15:01:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
