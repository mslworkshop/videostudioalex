<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->string('id',16)->uniq();
            $table->string('file_name',120);
            $table->integer('file_type')->default(0);
            $table->string('file_mime');
            $table->integer('file_size');
            $table->integer('file_position')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('pushtime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
