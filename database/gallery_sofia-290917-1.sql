-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 29, 2017 at 12:11 AM
-- Server version: 5.7.18
-- PHP Version: 7.0.21

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;

--
-- Database: `gallery_sofia`
--

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text1` text COLLATE utf8mb4_unicode_ci,
  `text2` text COLLATE utf8mb4_unicode_ci,
  `type` int(11) NOT NULL DEFAULT '0',
  `file_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'off',
  `priority` int(11) NOT NULL DEFAULT '1',
  `pushtime` int(16) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `files`
--

TRUNCATE TABLE `files`;
-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `video_url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `gallery`
--

TRUNCATE TABLE `gallery`;
--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `description`, `video_url`, `created_at`, `updated_at`) VALUES
('250632', 'Test 1', 'Описанието което ще е тук<br>', '3CcM30H_lsY', '2017-09-28 19:43:49', '2017-09-28 19:43:49'),
('250821', 'Тестът #2', '<p>Традиционна българска сватба 02.09.2017 <br></p>', 'B6AxXg9y7pI', '2017-09-28 20:09:24', '2017-09-28 20:09:24'),
('250960', 'Нова Галерия', '<h1 class=\"title style-scope ytd-video-primary-info-renderer\">Сватба на Ники и Венеция гр Лом орк К2 и Румен Борилов </h1>', 'KcRcEkGle0k', '2017-09-28 21:56:41', '2017-09-28 21:56:41'),
('250971', 'Папараци', '<p> Тайното кръщене на сина на Емануела</p><p><br><h4 id=\"title\" class=\"style-scope ytd-metadata-row-renderer\">\r\n      Category<a class=\"yt-simple-endpoint style-scope yt-formatted-string\" href=\"https://www.youtube.com/channel/UCi-g4cjqGV7jvU8aeSuj0jQ\"> Entertainment</a>\r\n      \r\n    </h4><h4 id=\"title\" class=\"style-scope ytd-metadata-row-renderer\">\r\n      License <span style=\"background-color: rgb(255, 255, 0);\"><font face=\"Comic Sans MS\">Standard YouTube License</font></span><br></h4></p>', 'ldh8A6bBryc', '2017-09-28 22:01:32', '2017-09-28 22:01:32');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` tinyint(4) NOT NULL,
  `action` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pushtime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `history`
--

TRUNCATE TABLE `history`;
--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `uid`, `action`, `data`, `pushtime`) VALUES
(1, 1, 'Login', '{\"user_addr\":null}', 1505172190),
(2, 1, 'Logout', '{\"user_addr\":\"10.8.133.13\"}', 1505172328),
(3, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505172621),
(4, 1, 'Logout', '{\"user_addr\":\"10.8.133.13\"}', 1505177433),
(5, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505224944),
(6, 1, 'Logout', '{\"user_addr\":\"10.8.133.13\"}', 1505225116),
(7, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505225125),
(8, 1, 'Logout', '{\"user_addr\":\"10.8.133.13\"}', 1505225149),
(9, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505225158),
(10, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505257041),
(11, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505500170),
(12, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505509303),
(13, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505519607),
(14, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505524593),
(15, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505574077),
(16, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505624372),
(17, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505645727),
(18, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505647371),
(19, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505663845),
(20, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505710600),
(21, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505728685),
(22, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505754583),
(23, 1, 'Login', '{\"user_addr\":\"91.92.20.115\"}', 1505769430),
(24, 1, 'Logout', '{\"user_addr\":\"10.8.133.13\"}', 1505772568),
(25, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505772582),
(26, 1, 'Logout', '{\"user_addr\":\"10.8.133.13\"}', 1505773033),
(27, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505773043),
(28, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505779844),
(29, 1, 'Login', '{\"user_addr\":\"91.92.20.115\"}', 1505800120),
(30, 1, 'Login', '{\"user_addr\":\"213.231.165.131\"}', 1505812330),
(31, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505837286),
(32, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1505919578),
(33, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1506009927),
(34, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1506082544),
(35, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1506268640),
(36, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1506544576),
(37, 1, 'Login', '{\"user_addr\":\"10.8.133.13\"}', 1506614031);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(25, '2014_10_12_000000_create_users_table', 1),
(26, '2014_10_12_100000_create_password_resets_table', 1),
(27, '2017_08_31_051204_create_upload_table', 1),
(29, '2017_09_11_233817_user_history', 2),
(32, '2017_09_17_085239_files_table', 5),
(33, '2017_08_31_051211_create_gallery_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `password_resets`
--

TRUNCATE TABLE `password_resets`;
-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `file_id` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_mime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `pushtime` int(11) NOT NULL,
  `gallery_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `uploads`
--

TRUNCATE TABLE `uploads`;
--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`file_id`, `file_name`, `file_type`, `file_mime`, `file_size`, `pushtime`, `gallery_id`) VALUES
('19ad1666d0cf', '19ad1666d0cf_803f95a7eaaf91f26668420f037ed95b.jpg', 'image', 'image/jpeg', 58296, 1506624892, '250960'),
('6e6d23c899c0', '6e6d23c899c0_2af6b6c762f430012919d2b573b23603.png', 'image', 'image/png', 156053, 1506625227, '250971'),
('84d8f74826b7', '84d8f74826b7_e3cd0ea182610f93abef13ad68bc83ba.jpg', 'image', 'image/jpeg', 61662, 1506616939, '250632'),
('b34aec8b2ea1', 'b34aec8b2ea1_3f49cf08bf0eac98e44773afd8674e00.jpg', 'image', 'image/jpeg', 72708, 1506618561, '250821'),
('db8ee8769338', 'db8ee8769338_3d2bb7e7c923ef434911f71be5828150.png', 'image', 'image/png', 147185, 1506625228, '250971');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ctrlCode` int(11) NOT NULL DEFAULT '0',
  `last_user_ip` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `ctrlCode`, `last_user_ip`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Buffer Petroff', 'admin@unix-ltd.net', '$2y$10$YGu2a6G0w/LY3X.FpD4hPOciCN785QrMeOc3uA0oAowP636Kpgdp.', 1, '10.8.133.13', 'BL9DPcA5AP09MbL2yHv6HAJjO69n3zjspsTmpWeT3flvOV118Hgmvy4bIubj', '2017-09-08 18:01:37', '2017-09-19 19:08:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;SET FOREIGN_KEY_CHECKS=1;
COMMIT;
