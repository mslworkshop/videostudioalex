<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
             App\User::create([
            'name' => 'Admin',
            'email' => 'admin@unix-ltd.net',
            'password' => bcrypt('pwd123'),
            'ctrlCode' => 1,
        ]);
    }
}
