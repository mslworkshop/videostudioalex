<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/phpinfo', function() { phpinfo(); } );

Auth::routes();

Route::get('/settings', ['as' => 'doSettings', 'uses' => 'HomeController@showSettingsPanel']);
Route::post('/settings2', ['as' => 'usermod', 'uses'=> 'UserDataController@saveUserData']);

route::group(['prefix' => 'gallery'], function (){
route::get('/', ['as'=>'gallery_index', 'uses' =>'GalleryController@view_Gallery']);
route::get('view/{id}', 'GalleryController@view_byID');
route::post('edit', ['as' => 'editGalleryEelement', 'uses' => 'GalleryController@editGallery']);
});

route::group(['prefix' => 'reviews'], function(){
route::get('/', ['as'=>'reviews_index', 'uses'=>'ReviewController@Home']);
route::get('view/{id}', 'ReviewController@ViewbyID');
});


route::get('/video', ['as' => 'video_index', 'uses' => 'VideoController@Home']);

route::group(['prefix' => '/api'], function(){

route::post('/addGalleryPhotos', 'GalleryController@addNewPhotos');
route::post('/addGallery', 'GalleryController@GalleryManager');
route::post('/editGallery', 'GalleryController@GalleryManager');

route::post('/addVideo', 'VideoController@VideoManager');
route::post('/editVideo', 'VideoController@VideoManager');

route::post('addReviewPhoto', 'ReviewController@addNewPhoto');
route::post('addReview', 'ReviewController@ReviewManager');
route::post('editReview',  'ReviewController@ReviewManager');

route::get('deleteGallery/{id}', 'GalleryController@deleteGallery');
route::get('deleteVideo/{id}', 'VideoController@deleteVideo');
route::get('deleteReview/{id}', 'ReviewController@deleteReview');
route::post('deletePicture/{id}', 'UploadController@deletePhotoByID');

});

Route::get('/home', function(){ return redirect('/gallery'); });
