

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-10 col-md-5 col-sm-3 col-xs-3 col-sm-offset-1">
			
			<form role="form"  method="POST" id="EditVideoElement-{{$data->id}}" name="EditVideoElement-{{$data->id}}" >
			<input type="text" hidden="hidden" name="bankID" value="{{$data->id}}"></input>

				<div class="form-group">
					<label class="label label-default" for="InputTitle">Title</label>
					<input class="form-control" id="InputTitle" type="text" name="title" value="{{$data->title}}" />
				</div>
				<div class="form-group">
					<label class="label label-default" for="InputLink">Video Link</label>
					<input class="form-control" id="InputLink" type="text" name="video" value="{{$data->video}}" />
				</div>
	
<img class="img-thumbnail" src="https://img.youtube.com/vi/{{$data->video}}/hqdefault.jpg" height="99%" width="100%" >

<p class="text-right" style="margin-top: 10px;">Last update: <cite>{{$data->updated_at}}</cite></p>

	<div class="form-group col-lg-12">
				<button type="submit" name="submit" id="editSubmitVideo" class="btn btn-default col-md-12">Edit Video</button>
	</div>


			</form>

			<div class="form-group col-md-12">
				<pre id="status_report_editVideo-{{$data->id}}" class="btn-default" style="visibility: hidden;" onclick="ElementHide(this)" title="Click to hide" data-toggle="tooltip"></pre>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
                $("#EditVideoElement-{{$data->id}}").submit(function(event){
                event.preventDefault();
                var divID = "#status_report_editVideo-{{$data->id}}";

                $.ajax({
                    method: 'POST',
                    url: '/api/editVideo',
                    dataType: "json",
                   // contentType: "application/json charset=UTF-8",

                      contentType: "application/x-www-form-urlencoded",
                    data : $('#EditVideoElement-{{$data->id}}').serializeArray() ,
                    success: function(data){
                    if(data.status == "danger"){
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        }else {
                            $(divID).html(data.report);
                        }
                    } else {
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        } else {
                            $(divID).html(data.report);
                        }
                    }
                        setClass(divID, data.status)
                        $(divID).visible();
                        $(divID).sf();
                    },
                    error: function(xhr, desc, err){
                    if(err.status == "danger"){
                        $(divID).html(err.report+"::<br>"+err.exec+" ("+data.ID+")");
                    } else {
                        $(divID).html(err.report);
                    }
                        setClass(divID, err.status)
                        $(divID).visible();
                        $(divID).sf();
                    },
                       complete: function() {
                         setTimeout(function(){ window.location="/video" }, 1000);
                      }
                });
                
            });
});
</script>