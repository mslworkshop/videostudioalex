@extends('layouts.app')

@section('content')
<div class="row">
<div class="container-fluid">
@foreach($Query as $data)
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
<div class="img-thumbnail" style="margin: 30px;">
<div class="text-muted"><h4>{{$data->title}}</h4></div>
<a href="http://youtube.com/watch?v={{$data->video}}" class="btn btn-default" target="_blank">
<img class="img-thumbnail" src="https://img.youtube.com/vi/{{$data->video}}/hqdefault.jpg" width="90%" >
</a>
	<p class="text-right" style="margin-top: 10px;">
<a href="#video-edit-{{$data->id}}" role="button" class="btn btn-primary" data-toggle="modal"><i class="fa fa-pencil-square-o" aria-hidden="true"  title="Edit" data-toggle="tooltip"></i></a>
<a class="btn btn-danger" href="api/deleteVideo/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true" title="Remove" data-toggle="tooltip"></i></a></p>
<p class="text-left">
</p>
</div>
</div>
@include('modal.edit-video')
@endforeach
	</div>
			<center>
				{{$Query->links()}}
			</center>
	</div>


		@if (Session::has('status_report'))
		<div class="col-xs-8 col-md-offset-3">
				<pre class="col-xs-8 alert-dismissable">
                        <strong>{{ Session::get('status_report') }}</strong>
                </pre>
        </div>
        @endif
@endsection