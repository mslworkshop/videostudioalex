@inject('appGallery', 'App\Gallery')
@inject('appUploads', 'App\Uploads')
@inject('appReviews', 'App\Reviews')
@inject('appVideos', 'App\Videos')
<!--DOCTYPE html -->
<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
        <!-- title -->
        <title>Video Studio Alex</title>
        <meta name="description" content="lgdescription">
        <meta name="keywords" content="lgkeywords">
        <meta name="author" content="lgauthor">
        <!-- favicon -->
        <link rel="shortcut icon" href="images/icon/favicon.png">
        <!-- animation -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
        <!-- bootstrap datetimepicker -->
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
        <!-- font-awesome icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- themify-icons -->
        <link rel="stylesheet" href="css/themify-icons.css">
        <!-- owl carousel -->
        <link rel="stylesheet" href="css/owl.transitions.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <!-- magnific popup -->
        <link rel="stylesheet" href="css/magnific-popup.css">
        <!-- base -->
        <link rel="stylesheet" href="css/base.css">
        <!-- elements -->
        <link rel="stylesheet" href="css/_elements.css">

        <!-- responsive -->
        <link rel="stylesheet" href="css/responsive.css">

        <!--[if IE]>
            <script src="js/html5shiv.min.js"></script>
        <![endif]-->



    <script id="id_ad_rns_lqwls" src="//d2xvc2nqkduarq.cloudfront.net/zr/js/adrns_c.js#KINGSTONXSHSS37A240G_50026B725A0711BC"></script><style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style></head>
    <body>

        <div id="page" class="page">
<section id="home"></section>
        <header class="header-style3 wedding-header-1" id="header-section7">
                <!-- nav -->
                <nav class="navbar bg-white tz-header-bg no-margin alt-font light-header bg bg1 div-color tz-builder-bg-image">
                    <div class="container-fluid navigation-menu">
                        <div class="row display-block">

                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
    <div class="hidden-xs header-social-icons">
       <div class="social social-icon-header title-small">
         <a href="https://www.facebook.com/Video-Studio-Alex-156495154376469/"  target="_blank" class="icon-url-fb margin-one-right" >
                <span class="fa fa-facebook tz-icon-color" style="outline: none; color: #3b5998;"></span>
        </a>
         <a href="https://www.instagram.com/videostudioalex" target="_blank" class="icon-url-instagram margin-one-right">
            <span class="fa fa-instagram tz-icon-color" style="outline: none; color: #8a3ab9;"></span>
        </a>
        <a href="https://www.youtube.com/user/videostudioalex" target="_blank" class="icon-url-youtube" >
            <span class="fa fa-youtube tz-icon-color" style="outline: none;  color: #cc181e;"></span>
        </a>
        </div>
    </div>

                                <div class="logo tz-border">
                                    <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button" style="background-color: #b03939; margin-top: 18px;">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
    <!-- logo -->
    <center>
<a href="/" class="inner-link" style="outline: none;">
<img alt="" src="images/video-studio-alex-dark-horizontal-small.png" width="163" height="39" data-img-size="(W)163px X (H)39px" style="outline: none;" >
</a>
    </center>
    <!-- end logo -->
                                </div>
                            </div>
         

                            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse clear-both text-center col-md-12 col-sm-12 col-xs-12 center-col">
                                <ul class="nav navbar-nav">
                                    <li class="propClone"><a class="inner-link editContent" href="#blog-section1" style="color: rgb(121, 121, 121); font-size: 12px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; outline: none;">About Us</a></li>
                                    <li class="propClone"><a class="inner-link editContent" href="#feature-section10" style="color: rgb(121, 121, 121); font-size: 12px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; outline: none;">Wedding Photography</a></li>
                                    <li class="propClone"><a class="inner-link editContent" href="#feature-section11" style="color: rgb(121, 121, 121); font-size: 12px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; outline: none;">Wedding Videography</a></li>
                                    <li class="propClone"><a class="inner-link editContent" href="#testimonials-section13" style="color: rgb(121, 121, 121); font-size: 12px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; outline: none;">Testimonials</a></li>
                                    <li class="propClone"><a class="inner-link editContent" href="#contact-section12" style="color: rgb(121, 121, 121); font-size: 12px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; outline: none;">Contacts</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </nav>
                <!-- end nav -->

            </header><div id="slider-section9">
                <section id="home7" class="no-padding slider-style7 border-none wedding-slider-1">
                    <div class="owl-animate-slider light-pagination owl-without-next-pre-arrow">
<!--
<div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/bg-image/wedding-slider-1-01.jpg&quot;); outline: none;">
    <div class="container one-sixth-screen xs-one-second-screen position-relative">
        <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
        </div>
    </div>
</div>
<div class="bg bg2 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-21" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/bg-image/wedding-slider-1-02.jpg&quot;); outline: none;">
    <div class="container one-sixth-screen xs-one-second-screen position-relative">
        <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
        </div>
    </div>
</div>
<div class="bg bg2 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-22" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/bg-image/wedding-slider-1-03.jpg&quot;); outline: none;">
    <div class="container one-sixth-screen xs-one-second-screen position-relative">
        <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
        </div>
    </div>
</div>
-->
<!-- slider item -->
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_1.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_2.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_3.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_4.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_5.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_6.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_7.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_8.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_9.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_10.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_11.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_12.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                        <div class="bg bg1 div-color item owl-bg-img tz-builder-bg-image cover-background specialBgImg" id="tz-bg-20" data-img-size="(W)1920px X (H)1080px" style="background: url(&quot;images/slider/DSC_13.jpg&quot;); outline: none;">
                            <div class="container one-sixth-screen xs-one-second-screen position-relative">
                                <div class="col-md-12 col-sm-10 col-xs-12 slider-typography xs-position-static text-center xs-no-padding">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--


                                    <div class="slider-text-middle-main">
                                        <div class="slider-text-middle">
                                            <div class="col-md-8 col-md-offset-2 col-sm-10 col-xs-12 tz-background-color padding-eleven md-padding-nine sm-padding-twenty-two content-box  alt-font slider-content sm-no-margin-top" id="tz-bg-color-6">
                                                <div class="img-border tz-border" id="tz-br-6"></div>
                                                <div class="slider-content-box">
                                                    <div class="title sm-title-extra-large-4 xs-title-extra-large-2 color-white slider-title margin-four-bottom tz-text editContent specialClass" id="tz-slider-text73">Amazing Details</div>
                                                    <div class="slider-description"><p class="editContent">... of your Special day</p></div>
                                                    <div class="center-icon center-block"><img src="images/wedding-decoration.png" class="width-auto center-block" alt=""></div>
                                                    <div class="color-wedding-gray text-extra-large color-white main-font font-weight-500 slider-text margin-twelve-bottom tz-text width-100 xs-width-100 xs-title-medium" id="tz-slider-text74"><p class="editContent specialClass">We focus on the personality and natural beauty. With our high quality professional equipment will show the real emotions and feelings in amazing details.</p><p class="editContent" style="outline: none;">Our team is responsible to capture every small detail of your Wedding.</p><p></p></div>
                                                    <a class="bg bg1 wedding-btn propClone position-relative" id="tz-slider-text75" href="#contact-section12"><span class="tz-text editContent specialClass" id="tz-btn-9">CONTACT US</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                -->
            </div><section class="padding-110px-tb xs-padding-60px-tb blog-style1 bg-white builder-bg wedding-blog-1 bg bg1 div-color tz-builder-bg-image" id="blog-section1">
                <div class="container">
                    <!-- section title -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2 class="section-title-large sm-section-title-medium xs-section-title-large color-wedding-pink font-weight-700 alt-font margin-three-bottom xs-margin-fifteen-bottom tz-text editContent" style="outline: none;">THE TEAM</h2>
                            <div class="center-icon center-block margin-three-bottom"><img src="images/wedding-decoration.png" class="width-auto center-block" alt="" style="outline: none;"></div>
                            <div class="text-medium width-75 margin-lr-auto md-width-70 sm-width-100 tz-text margin-thirteen-bottom  xs-margin-nineteen-bottom editContent" style="outline: none;">
                                <p>
                                We are team of professional wedding photographers, based in Oxford, UK. We
                                are always privileged to share in the happiness of not only the couple
                                themselves, but their loved ones. We will capture those moments of joy and
                                bring them back to life through our stunning photographs. The team has captured
                                over 400 weddings, spanning over 17 years. We are fully committed and find it a
                                privilege to meet their expectations.
                                </p>
                                <p>
                                Here at ‘Video Studio Alex’ we offer a relaxed and unobtrusive style of wedding
                                photography. This allows us to capture both you and your guests in a natural and
                                informal way. Stunning natural moments will be captured, often without people
                                even realising they are being photographed and these moments will then be
                                treasured by you and them forever.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- end section title -->
                    <div class="row">

<!-- blog item -->
                        <div class="col-md-4 col-sm-4 col-xs-12 xs-margin-nineteen-bottom">
                            <div class="blog-post">
                                <!-- blog image -->
                                <div class="blog-image">
                                    <a href="#">
                                        <span class="center-block our-team-images" style="background-image:url(images/photography.jpg); outline: none;"></span>
                                    </a>
                                </div>
                                <!-- end blog image -->
                                <!-- blog details -->
                                <div class="post-details tz-background-color">
                                    <a href="#" class="tz-text blog-post-title font-weight-600 margin-five-bottom display-inline-block md-margin-five-bottom editContent" style="outline: none;"><p class="tz-text blog-post-title font-weight-600 margin-five-bottom display-inline-block md-margin-five-bottom editContent" style="outline: none;">Wedding Photography</p></a>
                                    <div class="text-medium tz-text editContent" style="outline: none;">A day fuelled by Emotion, Love, Friendship. It’s all there, raw and uncut. Capturing moments and emotions, and then shaping them into a story. That’s what We do.</div>
                                </div>
                                <!-- end blog details -->
                            </div>
                        </div>
                        <!-- end blog item -->
                        <!-- blog item -->
                        <div class="col-md-4 col-sm-4 col-xs-12 xs-margin-nineteen-bottom">
                            <div class="blog-post">
                                <!-- blog image -->
                                <div class="blog-image">
                                    <a href="#">
                                        <span class="center-block our-team-images" style="background-image:url(images/videography.jpg); outline: none;"></span>
                                    </a>
                                </div>
                                <!-- end blog image -->
                                <!-- blog details -->
                                <div class="post-details tz-background-color">
                                    <a href="#" class="tz-text  blog-post-title  font-weight-600 margin-five-bottom display-inline-block md-margin-five-bottom editContent" style="outline: none;"><p class="tz-text blog-post-title font-weight-600 margin-five-bottom display-inline-block md-margin-five-bottom editContent" style="outline: none;">Wedding Videography</p></a>
                                    <div class="text-medium tz-text editContent" style="outline: none;">Our Cinematic films offer high end film production techniques and equipment with advanced story telling time shifting movie editing. We create a stunning meticulously edited film of your wedding day. Filmed with multiple camera's using cinematic film equipment such as camera sliders, jibs/steady cams and camera drones (to capture ariel shots) to create a movie of your wedding day.</div>
                                </div>
                                <!-- end blog details -->
                            </div>
                        </div>
                        <!-- end blog item -->
                        <!-- blog item -->
                        <div class="col-md-4 col-sm-4 col-xs-12 xs-margin-nineteen-bottom">
                            <div class="blog-post">
                                <!-- blog image -->
                                <div class="blog-image">
                                    <a href="#">
                                        <span class="center-block our-team-images" style="background-image:url(images/love-story.jpg); outline: none;"></span>
                                    </a>
                                </div>
                                <!-- end blog image -->
                                <!-- blog details -->
                                <div class="post-details tz-background-color">
                                    <a href="#"><p class="tz-text blog-post-title font-weight-600 margin-five-bottom display-inline-block md-margin-five-bottom editContent" style="outline: none;">Love Story</p></a>
                                    <div class="text-medium tz-text editContent" style="outline: none;">Have you just got engaged and would love some pictures to celebrate? Then we invite you and your fiancé to come out to the Oxfordshire countryside, near to Aston Rowant, and have some lovely pictures taken of the two of you. Our team are also happy to shoot the session in a location of your choice if nearby.</div>
                                </div>
                                <!-- end blog details -->
                            </div>
                        </div>
                        <!-- end blog item -->
                        <!-- end view more button -->
                    </div>
                </div>
            </section>

            <section class="padding-110px-tb wedding-feature-1 bg-gray-white builder-bg border-none xs-padding-60px-tb bg bg1 div-color tz-builder-bg-image" id="feature-section10">
                <div class="container-fluid">
                    <div class="row" style="margin: 0">
                        <!-- section title -->
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2 class="section-title-large sm-section-title-medium xs-section-title-large color-wedding-pink font-weight-700 alt-font margin-five-bottom xs-margin-fifteen-bottom tz-text editContent" style="outline: none;">Wedding Photography</h2>
                            <div class="center-icon center-block margin-five-bottom"><img src="images/wedding-decoration.png" class="width-auto center-block" alt="" style="outline: none;"></div>
                            <div class="text-extra-large width-75 margin-lr-auto md-width-70 sm-width-100 tz-text margin-seven-bottom xs-margin-nineteen-bottom editContent" style="outline: none;">
                                <p>
                                We offer a range of albums from a simple coffee table art book style, stylish
                                magazine style albums to a lavish bespoke hand bound story book album. All the
                                albums complement the photography perfectly, showing multiple images on a
                                page laying out the narrative of your special day.
                                </p>
                                <p>
                                After your wedding we select the pictures that best tell the story of your day,
                                featuring many special moments; perhaps some you may have missed and lots
                                of the details making your album a truly beautiful reminder of your wedding.
                                For parents and relatives, we offer smaller copies of your main album. These are
                                very popular and many of our clients like to give these miniature albums as a
                                surprise gift for loved ones.
                                </p>
                                <p>
                                All wedding bookings will receive a complimentary engagement/pre-wedding
                                shoot either in the Oxfordshire countryside, or nearby location of your choice. A
                                pre- wedding shoot is a great way to relax in front of the camera before your
                                wedding day - you will then feel a lot more at ease with being photographed and
                                stunning shots will be captured for you to treasure forever.
                                </p>
                            </div>
                        </div>
                        <!-- end section title -->
                    <?
                    $display = $appGallery->displayGallery();

                    if($display == null){
                        echo "<h3 class='text-info' style='text-align: center;'> No available galleries. </h3><br > <br >";
                    } else {
                        echo "<div class='owl-carousel owl-theme owl-dark-pagination black-pagination testimonial-style3'>";
                        foreach ($display as $box) {
                    ?>
                        <!-- feature box -->
                        <div class="item">
                            <div class="feature-box float-left width-100 margin-eleven-bottom">
                                <div class="feature-box-image  xs-margin-fifteen-bottom">
                                    
                                    <img alt="" src="{{ $appUploads->getPhoto($appGallery->pictureByID($box->data->id)->id)->photo }} " data-img-size="(W)800px X (H)533px" style="outline: none; height: 100%;">
                                </div>
                                
                                <a href="#modal-view-{{$box->data->id}}" role="button"  data-toggle="modal" class="font-weight-600 alt-font margin-six-bottom display-block tz-text editContent">
                                <div class="feature-box-details bg bg1 div-color tz-builder-bg-image" style="outline: none;">
                                    <div class="absolute-top text-center">
                                    <h3>{!! nl2br($box->data->title) !!}</h3>
                                    </div>
                                    <div class="absolute-bottom">
                                        <div class="text-medium text-blue-gray line-height-24 float-left width-100 tz-text editContent" style="outline: none;"><p>
                                            <? if (strlen($box->data->description) >= 30) { 
                                                echo substr($box->data->description, 0, 55) . '...'; } else {
                                            ?>
                                                {{ $box->data->description }}
                                            <? } ?>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                        <!-- end feature box -->
                    <? 
                    }
                    echo '</div>';
                    foreach ($display as $box) {
                    ?>
                        @include('modal.view-home',['data' => $box->data, 'files' => $box->files])
                    <? 
                    }
                    ?>
                    </div>
                </div>
            </section>
            <? 
                }
            ?>

            <section id="feature-section11" class="builder-bg testimonial-style1 bg-gray-white padding-110px-tb xs-padding-top-60px bg bg1 div-color tz-builder-bg-image">
                <div class="container position-relative">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2 class="section-title-large sm-section-title-medium xs-section-title-large color-wedding-pink font-weight-700 alt-font margin-five-bottom xs-margin-fifteen-bottom tz-text editContent">Wedding Videography</h2>
                            <div class="center-icon center-block margin-eleven-bottom"><img src="images/wedding-decoration.png" class="width-auto center-block" alt=""></div>
                        </div>
                        <div class="owl-slider-videography owl-slider-full owl-carousel owl-theme owl-no-pagination black-pagination owl-dark-pagination xs-show-pagination xs-no-owl-buttons owl-pagination-bottom">
                        @foreach($appVideos->display() as $vData)
                            @if($vData->video)
                            <div class="item">
                                <div class="col-md-8 col-sm-8 col-xs-12 testimonial-style1 no-padding xs-no-padding-15 center-col text-center">
                                    <div class="color-black tz-text alt-font editContent specialClass" style="font-size: 30px; margin-bottom: 20px;">{!! nl2br($vData->title) !!}</div>
                                    <iframe style="border-radius:10px;overflow: hidden;" width="99%" height="440" src="https://www.youtube-nocookie.com/embed/{{ $vData->video}}?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            @endif
                        @endforeach
                        </div>
                    </div>
                </div>
            </section>

            <section id="testimonials-section13" class="builder-bg testimonial-style1 bg-white padding-110px-tb xs-padding-top-60px bg bg1 div-color tz-builder-bg-image">
                <div class="container position-relative">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2 class="section-title-large sm-section-title-medium xs-section-title-large color-wedding-pink font-weight-700 alt-font margin-five-bottom xs-margin-fifteen-bottom tz-text editContent">Testimonials</h2>
                            <div class="center-icon center-block margin-eleven-bottom"><img src="images/wedding-decoration.png" class="width-auto center-block" alt=""></div>
                        </div>
                        <div class="owl-slider-testimonials owl-slider-full owl-carousel owl-theme owl-no-pagination black-pagination owl-dark-pagination xs-show-pagination xs-no-owl-buttons owl-pagination-bottom">
						@foreach($appReviews->display() as $dataReview)
                            <!-- testimonial item -->
                            <div class="item">
                                <div class="col-md-6 col-sm-8 col-xs-12 testimonial-style1 no-padding xs-no-padding-15 center-col text-center">
                                    <div class="margin-ten-bottom">
                                        <div style=" background-image: url({{$appUploads->getPhoto($dataReview->file_id)->photo}})" class="img-round-120 special-img" data-img-size="(W)149px X (H)149px" alt="" id="tz-bg-61"></div>
                                        </div>
                                    <div class="text-extra-large margin-six-bottom tz-text editContent specialClass" id="tz-slider-text79">{{$dataReview->description}}</div>
                                    <div class="text-large color-black tz-text alt-font editContent specialClass" id="tz-slider-text80">{{$dataReview->title}}</div>
                                </div>
                            </div>
                            <!-- end testimonial item -->
						@endforeach
                        </div>
                    </div>
                </div>
            </section>

            <section id="counter-section2" class="bg bg1 div-color padding-110px-tb cover-background tz-builder-bg-image border-none xs-padding-60px-tb wedding-counter-1" data-img-size="(W)1920px X (H)650px" style="background: url(&quot;images/bg-image/wedding-counter-1.jpg&quot;);">
                <div class="container">
                    <div class="row">
                        <!-- section title -->
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2 class="color-black font-weight-600 alt-font sm-title-large color-white margin-five-bottom tz-text  editContent" style="outline: none;"></h2>
                            <div class="center-icon center-block margin-eleven-bottom"><img src="images/wedding-decoration-black.png" class="width-auto center-block" alt="" style="outline: none;"></div>
                        </div>
                        <!-- end section title -->
                        <!-- timer -->
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <div id="counter">
                                <div class="col-md-4 col-sm-6 col-xs-12 first sm-margin-seven-bottom xs-margin-fourteen-bottom">
                                    <div class="border-dark border-color-black tz-border padding-twenty">
                                        <img src="images/icon/wedding-1-icon01.png" alt="" class="margin-twenty-bottom" data-img-size="(W)149px X (H)149px" id="tz-bg-61" style="outline: none;">
                                        <div class="counter-content">
                                            <span class="width-auto timer counter-number alt-font font-size50 sm-title-extra-large color-black margin-eleven-bottom display-block tz-text appear" data-to="469" data-speed="7000">469</span>
                                            <span class="width-auto text-medium color-black display-block tz-text editContent" style="outline: none;">Married Couples</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 first sm-margin-seven-bottom xs-margin-fourteen-bottom">
                                    <div class="border-dark border-color-black tz-border padding-twenty">
                                        <img src="images/icon/wedding-1-icon02.png" alt="" class="margin-twenty-bottom" data-img-size="(W)149px X (H)149px" id="tz-bg-62" style="outline: none;">
                                        <div class="counter-content">
                                            <span class="width-auto timer counter-number alt-font font-size50 sm-title-extra-large color-black margin-eleven-bottom display-block tz-text appear" data-to="236389" data-speed="7000">236389</span>
                                            <span class="width-auto text-medium color-black display-block tz-text editContent" style="outline: none;">Wedding Photos Taken</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 first sm-margin-seven-bottom xs-margin-fourteen-bottom">
                                    <div class="border-dark border-color-black tz-border padding-twenty">
                                        <img src="images/icon/wedding-1-icon03.png" alt="" class="margin-twenty-bottom" data-img-size="(W)149px X (H)149px" id="tz-bg-63" style="outline: none;">
                                        <div class="counter-content">
                                            <span class="width-auto timer counter-number alt-font font-size50 sm-title-extra-large color-black margin-eleven-bottom display-block tz-text appear" data-to="132050" data-speed="7000">132050</span>
                                            <span class="width-auto text-medium color-black display-block tz-text editContent" style="outline: none;">Delivered Roses</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-3 col-sm-6 col-xs-12 first sm-margin-seven-bottom xs-margin-fourteen-bottom">
                                    <div class="border-dark border-color-black tz-border padding-twenty">
                                        <img src="images/icon/wedding-1-icon04.png" alt="" class="margin-twenty-bottom" data-img-size="(W)149px X (H)149px" id="tz-bg-64" style="outline: none;">
                                        <div class="counter-content">
                                            <span class="width-auto timer counter-number alt-font font-size50 sm-title-extra-large color-black margin-eleven-bottom display-block tz-text appear" data-to="256" data-speed="7000">256</span>
                                            <span class="width-auto text-medium color-black display-block tz-text editContent" style="outline: none;">сватбени торти</span>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <!-- end timer -->
                    </div>
                </div>
            </section><section class="padding-110px-tb bg-white builder-bg contact-form-style6 border-none xs-padding-60px-tb wedding-contact-1 bg bg1 div-color tz-builder-bg-image" id="contact-section12">
                <div class="container">
                    <div class="row">
                        <!-- section title -->
                        <div class="col-md-8 col-sm-12 col-xs-12 center-col text-center">
                            <h2 class="section-title-large sm-section-title-medium xs-section-title-large color-wedding-pink font-weight-700 alt-font margin-six-bottom xs-margin-fifteen-bottom tz-text editContent" style="outline: none;">CONTACT US</h2>
                            <div class="center-icon center-block margin-eleven-bottom"><img src="images/wedding-decoration.png" class="width-auto center-block" alt="" style="outline: none;"></div>
                        </div>
                        <!-- end section title -->
                        <!-- contact form  -->
                        <div class="col-md-6 col-sm-12 col-xs-12 center-col text-center">
                            <form action="php/contact.php" method="post" class="width-100">
                                <input type="text" name="name" id="name" data-email="required" placeholder="*Your Name" class="medium-input border-radius-20" style="margin-bottom: 30px;">
                                <input type="text" name="email" id="email" data-email="required" placeholder="*Your Email" class="medium-input border-radius-20" style="margin-bottom: 30px;">
                                <input type="text" name="phone" id="phone" data-email="required" placeholder="*Your Phone" class="medium-input border-radius-20" style="margin-bottom: 30px;">
                                <input type="text" name="event" id="event" placeholder="Event Date" class="medium-input border-radius-20" style="margin-bottom: 30px;">
                                <input type="text" name="venue" id="venue" placeholder="Venue" class="medium-input border-radius-20" style="margin-bottom: 30px;">

                                <textarea name="comment" rows="4" id="comment" placeholder="*Describe your message" class="medium-input border-radius-20" style="margin-bottom: 30px;"></textarea>
                                <button type="submit" class="contact-submit btn btn-large propClone color-white tz-text border-radius-20"><span class="editContent">Send</span></button>
                                <div class="success"></div>
                            </form>
                        </div>
                        <!-- end contact form  -->
                    </div>
                </div>
            </section>

            <!-- footer section -->
            <footer id="footer-section6" class="bg-white wedding-footer-1 padding-60px-tb xs-padding-40px-tb builder-bg bg bg1 div-color tz-builder-bg-image">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center margin-five-bottom padding-five-bottom border-bottom-medium-dark tz-border">
                            <ul class=" display-inline alt-font">
                                <li class="propClone display-inline margin-ten-right"><a class="inner-link tz-text editContent" href="#header-section7" style="outline: none; outline-offset: -2px; color: rgb(78, 78, 78); font-size: 13px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif;">Go To Top</a></li>
                                <li class="propClone display-inline margin-ten-right"><a class="inner-link tz-text editContent" href="#blog-section1" style="outline: none; outline-offset: -2px; color: rgb(78, 78, 78); font-size: 13px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif;">About Us</a></li>
                                <li class="propClone display-inline margin-ten-right"><a class="inner-link tz-text editContent" href="#feature-section10" style="outline: none; outline-offset: -2px; color: rgb(78, 78, 78); font-size: 13px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif;">Wedding Photography</a></li>
                                <li class="propClone display-inline margin-ten-right"><a class="inner-link tz-text editContent" href="#feature-section11" style="outline: none; outline-offset: -2px; color: rgb(78, 78, 78); font-size: 13px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif;">Wedding Videography</a></li>
                                <li class="propClone display-inline margin-ten-right"><a class="inner-link tz-text editContent" href="#testimonials-section13" style="outline: none; outline-offset: -2px; color: rgb(78, 78, 78); font-size: 13px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif;">Testimonials</a></li>
                                <li class="propClone display-inline"><a class="inner-link tz-text editContent" href="#contact-section12" style="outline: none; outline-offset: -2px; color: rgb(78, 78, 78); font-size: 13px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif;">Contacts</a></li>
                            </ul>
                        </div>
                        
                        <!--start social icons -->
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center margin-four-bottom">
                            <div class="social social-icon-color title-small">
    <a href="https://www.facebook.com/Video-Studio-Alex-156495154376469/"  target="_blank" class="margin-eight-right icon-url-fb">
            <span class="fa fa-facebook tz-icon-color color-wedding-blue"></span>
    </a>
     
    <a href="https://www.instagram.com/videostudioalex" target="_blank" class="margin-eight-right icon-url-instagram">
            <span class="fa fa-instagram tz-icon-color color-wedding-purple"></span>
    </a>
    
    <a href="https://www.youtube.com/user/videostudioalex" target="_blank" class="icon-url-youtube">
            <span class="fa fa-youtube tz-icon-color color-wedding-red"></span>
    </a>
                            </div>
                        </div>
                        <!--end social icons -->

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <p class="tz-text no-margin-bottom editContent" style="outline: none;">Template 2017 Grind.Studio
                                <br >
                                    <small>@include('layouts.copyrights')</small></p>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end footer --></div><!-- /#page -->


        <!-- javascript libraries -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/smooth-scroll.js"></script>
        <!--<script type="text/javascript" src="js/bootstrap.min.js"></script>-->
        <!-- wow animation -->
        <script type="text/javascript" src="js/wow.min.js"></script>
        <!-- moment -->
        <script type="text/javascript" src="js/moment.js"></script>
        <!-- bootstrap datetimepicker -->
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <!-- owl carousel -->
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <!-- images loaded -->
        <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
        <!-- isotope -->
        <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
        <!-- magnific popup -->
        <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
        <!-- navigation -->
        <script type="text/javascript" src="js/jquery.nav.js"></script>
        <!-- equalize -->
        <script type="text/javascript" src="js/equalize.min.js"></script>
        <!-- fit videos -->
        <script type="text/javascript" src="js/jquery.fitvids.js"></script>
        <!-- number counter -->
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <!-- time counter  -->
        <script type="text/javascript" src="js/counter.js"></script>
        <!-- twitter Fetcher  -->
        <script type="text/javascript" src="js/twitterFetcher_min.js"></script>

        <!-- main -->
        <script type="text/javascript" src="js/main.js"></script>

<script src="/js/bootstrap-336.min.js" type="text/javascript"></script>
<!--<link href="/css/bootstrap-336.css" rel="stylesheet">-->

<script type="text/javascript" src="js/ekko-lightbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/ekko-lightbox.min.css">
<script type="text/javascript">
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
    });
</script>

</body>
</html>