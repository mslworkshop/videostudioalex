<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Alex Video Gallery') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-checkbox.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="utils/font-awesome/css/font-awesome.min.css" type="text/css"/>
    <link rel="stylesheet" href="utils/facebox/facebox.css" type="text/css" />

    <!-- Scripts <script src="{{ asset('js/app.js') }}"></script>-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="bootstrap-fileinput/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="bootstrap-fileinput/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="bootstrap-fileinput/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="bootstrap-fileinput/js/fileinput.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <!-- include summernote css/js-->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>

<script type="text/javascript" src="js/ekko-lightbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/ekko-lightbox.min.css">
<script type="text/javascript">
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
    });
</script>

    <script src="utils/facebox/facebox.js" type="text/javascript"></script>
    <script type="text/javascript">

    

    jQuery(document).ready(function($) {
        $('a[rel*=facebox]').facebox();
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover(); 

        $('.modal').on('show.bs.modal', function (e) {
            var modal = $(e.target);
            var imgs = modal.find('img');
            imgs.each(function(){
                var imgEl = $(this);
                imgEl.attr('src', imgEl.attr('data-src'));
            });
        });
    });
    
    $('.selectpicker').selectpicker({
        style: 'btn-active',
        size: 4
    });
$(function(){
                $("#addGalleryElement").submit(function(event){
                event.preventDefault();
                var divID = "#status_report_addGallery";
                $.ajax({
                    method: 'post',
                    url: '/api/addGallery',
                    dataType: "json",
                    data: $('#addGalleryElement').serializeArray(),
                    contentType: "application/x-www-form-urlencoded",
                  
                    success: function(data){

                    if(data.status == "danger"){
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        }else {
                            $(divID).html(data.report);
                        }
                    } else {
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        } else {
                            $(divID).html(data.report);
                        }
                    }
                        setClass(divID, data.status)
                        $(divID).visible();
                        $(divID).sf();
                    },
                    error: function(xhr, desc, err){
                    if(err.status == "danger"){
                        $(divID).html(err.report+"::<br>"+err.exec+" ("+data.ID+")");
                    } else {
                        $(divID).html(err.report);
                    }
                        setClass(divID, err.status)
                        $(divID).visible();
                        $(divID).sf();
                    },  complete: function() {
                        window.location = "/gallery";
                    }
                });
            });

             $("#addReviewElement").submit(function(event){
                event.preventDefault();
                var divID = "#status_report_addReview";
                $.ajax({
                    method: 'POST',
                    url: '/api/addReview',
                    dataType: "json",
                    data: $('#addReviewElement').serializeArray(),
                    contentType: "application/x-www-form-urlencoded",
                  
                    success: function(data){

                    if(data.status == "danger"){
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        }else {
                            $(divID).html(data.report);
                        }
                    } else {
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        } else {
                            $(divID).html(data.report);
                        }
                    }
                        setClass(divID, data.status)
                        $(divID).visible();
                        $(divID).sf();
                    },
                    error: function(xhr, desc, err){
                    if(err.status == "danger"){
                        $(divID).html(err.report+"::<br>"+err.exec+" ("+data.ID+")");
                    } else {
                        $(divID).html(err.report);
                    }
                        setClass(divID, err.status)
                        $(divID).visible();
                        $(divID).sf();
                    },  complete: function() {
                        window.location = "/reviews";
                    }
                });
            });

                $("#addVideoElement").submit(function(event){
                event.preventDefault();
                var divID = "#status_report_addVideo";
                $.ajax({
                    method: 'POST',
                    url: '/api/addVideo',
                    dataType: "json",
                    data: $('#addVideoElement').serializeArray(),
                    contentType: "application/x-www-form-urlencoded",
                  
                    success: function(data){

                    if(data.status == "danger"){
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        }else {
                            $(divID).html(data.report);
                        }
                    } else {
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        } else {
                            $(divID).html(data.report);
                        }
                    }
                        setClass(divID, data.status)
                        $(divID).visible();
                        $(divID).sf();
                    },
                    error: function(xhr, desc, err){
                    if(err.status == "danger"){
                        $(divID).html(err.report+"::<br>"+err.exec+" ("+data.ID+")");
                    } else {
                        $(divID).html(err.report);
                    }
                        setClass(divID, err.status)
                        $(divID).visible();
                        $(divID).sf();
                    },  complete: function() {
                        window.location = "/video";
                    }
                });
            });

      });
        
    (function($) {
    $.fn.invisible = function() {
        return this.each(function() {
            $(this).css("visibility", "hidden");
        });
    };
    $.fn.visible = function() {
        return this.each(function() {
            $(this).css("visibility", "visible");
        });
    };

    $.fn.hd = function() {
        return $(this).fadeOut(800);
    };
    $.fn.sf = function() {
        return $(this).fadeIn(800);
    };

}(jQuery));

    function ElementHide(el) { $("#"+el.id).hd(); }
    function setClass(e,c) {  $(e).removeClass();      $(e).addClass('btn-'+c);  }
    </script>

</head>

<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Alex Video Gallery') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                        <li><a href="{{ route('gallery_index') }}"> Gallery <span class="badge">{{App\Gallery::count()}}</span></a></li>
                        <li><a href="{{ route('reviews_index') }}"> Reviews <span class="badge">{{App\Reviews::count()}}</span></a></li>
                        <li><a href="{{ route('video_index') }}"> Videos <span class="badge">{{App\Videos::count()}}</span></a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                        <li><a href="#">addNew..</a></li>
                        <li><a href="#modal-add-gallery" role="button"  data-toggle="modal"><b>GALLERY</b></a></li>
                        <li><a href="#modal-add-review" role="button"  data-toggle="modal"><b>REVIEW</b> </a></li>
                        <li><a href="#modal-add-video" role="button"  data-toggle="modal"><b>VIDEO</b></a></li>
                        <li>
                            <a href="{{ route('doSettings') }}" rel="facebox">{{ Auth::user()->name }}</a>
                        </li>
                        
                        <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out fa-3" aria-hidden="true"></i>
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                        </li>

                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
        <div class="footer">
            
                                    <h6>@include('layouts.copyrights')</h6>
        </div>
    </div>
        @include('modal.add-gallery')
        @include('modal.add-review')
        @include('modal.add-video')
</body>
</html>
