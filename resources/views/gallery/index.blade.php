@extends('layouts.app')
@inject('appGallery','App\Gallery')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			
			<? 
		$rowItems = $appGallery->dataRowbyPage();
		if($rowItems->items() != null) {
			$a=0;
foreach($rowItems as $data) {
	$a++;
$rowImage = $appGallery->pictureByID($data->id); 
	if(($a%4) == false) echo '<div class="row">';
	 ?>
				<div class="col-md-3">
					<div class="thumbnail">
					
					<div class="thumbnail">
							<img alt="{{$data->title}}" src="{{App\Http\Controllers\UploadController::retrivePathWithDate($rowImage->pushtime).$rowImage->file_name}}" style="width: 80%; height: 30%;" class="animated zoomIn" />
					</div>
					
						<div class="caption">
							<h3>
<?
if (strlen($data->title) >= 25) {
 echo substr($data->title, 0, 25) . '...';;
} else {
   echo $data->title;
}
 ?>
							</h3>

							<p>
							<?
							if (strlen($data->description) >= 30) {
							  echo substr($data->description, 0, 55) . '...';
							} else {
							  echo  $data->description;
							}
							?>
							</p>
							<p class="text-right">
<a href="#gallery-view-{{$data->id}}" role="button" data-toggle="modal" class="btn btn-info"><i class="fa fa-eye" title="Preview" data-toggle="tooltip"></i></a>
<a href="#gallery-edit-{{$data->id}}" role="button" class="btn btn-primary" data-toggle="modal"><i class="fa fa-pencil-square-o" aria-hidden="true"  title="Edit" data-toggle="tooltip"></i></a>
<a class="btn btn-danger" href="api/deleteGallery/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true" title="Remove" data-toggle="tooltip"></i></a></p>
<p class="text-left">
</p>		
							
							
						</div>

					</div>
				</div>

			@include('modal.edit-gallery', ['data' => $data, 'files' => $appGallery->GalleryFilesByID($data->id) ])
			@include('modal.view-gallery', ['data' => $data])
			
			<? 
					if(($a%4) == false)  echo '</div>';  
				 } 
			?>

			

			<center>
				{{$appGallery->dataRowbyPage()->links()}}
			</center>

		</div>
<?  } else { echo "<h3 class='text-danger' style='text-align: center;'>No available galleries.
		<br><br>
	<a href='#modal-add-gallery' role='button'  data-toggle='modal'><b class='btn btn-info'>Add new One?</b></a></h3>"; }
?> 

		@if (Session::has('status_report'))
		<div class="col-xs-8 col-md-offset-3">
				<pre class="col-xs-8 alert-dismissable">
                        <strong>{{ Session::get('status_report') }}</strong>
                </pre>
        </div>
        @endif

	</div>

</div>

@endsection