@inject('appUploads', 'App\Uploads')
<? $c=1; $is=false; ?>
<div class="container-fixed">
    <div class="row" style="padding-bottom: 3rem;">
 <!-- starting left column -->
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
            @foreach($files as $file)
                <? $img = $appUploads->getPhoto($file->id)->photo; if(($c % 3) == false){ $is=true;  } ?>
                
                @if($is)
                <div class="row">
                @endif
                <div class="col-lg-4 col-md-6 col-sm-4 col-xs-5">
                    <a href="{{$img}}" data-toggle="lightbox" data-gallery="gallery-{{$data->id}}" t="file_position">
                        <img data-src="{{$img}}" class="img-thumbnail"  class="img-fluid" style="height: 130px; margin-right: 1.1rem; margin-bottom: 1.5rem;">
                    </a>
                </div>
                @if($is)
                        </div>
                        @endif
                    <?
                    $c++;  $is = false; ?>
            @endforeach
</div>
 <!-- end left column -->
<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-10 col-xs-10">
            <blockquote>
            <p style="font-size: 14px;">
                {!! $data->description !!}
            </p>
            </blockquote>
        </div>
        <hr>
        <div class="row"></div>
        @if($data->video_url)
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <iframe width="99%" height="220" src="https://www.youtube-nocookie.com/embed/{{$data->video_url}}?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
        @endif
</div>

</div>
</div>

