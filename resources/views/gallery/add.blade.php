
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-10 col-sm-offset-1">
			
			<form role="form"  method="POST" id="addGalleryElement" name="addGalleryElement" >

		<!--formenctype="multipart/form-data"-->
<input type="text" hidden="hidden" name="bankIDGallery" id="bankIDGallery" value="<?  echo substr((time()%rand(9000,9999)^0x01f01337), 2, 4); ?>">
<input type="text" hidden="hidden" name="exec" value="create"></input>
<input type="text" hidden="hidden" name="package" id="packageGallery" value="sys.gallery"></input>

				<div class="form-group">
					<label class="label label-default" for="exampleInputEmail">Title</label>
					<input class="form-control" id="exampleInputEmail" type="text" name="title" />
				</div>

				<div class="form-group">
					<span class="label label-default" for="DescriptionArea">Description</span>
					<textarea class="form-control" name="description" id="DescriptionArea" style="width: 100%; height: 277px;"></textarea>
				</div>

				<div class="form-group">
					<span class="label label-default" for="VideoArea">YouTube Video ID</span>
					<input class="form-control" name="video_url" id="VideoArea" />
				</div>

				<div class="form-group">
					<label class="label label-default" for="InputFiles">Upload photos</label>
            <input id="InputFiles" name="files[]" class="file" type="file" multiple data-preview-file-type="text" data-upload-url="/api/addGalleryPhotos">
         			<p>
                        <small>Extensions: .png .gif .jpeg .jpg .bmp</small>
                    </p>
				</div>

	<div class="form-group col-lg-12">
				<button type="submit" name="submit" id="addGallery" class="btn btn-default col-md-12">Add Gallery</button>
	</div>


			</form>
			<div class="form-group col-md-12">
				<pre id="status_report_addGallery" class="btn-default" style="visibility: hidden;" onclick="ElementHide(this)" title="Click to hide" data-toggle="tooltip"></pre>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $('.summernote').summernote({
      height: 200,                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null,             // set maximum height of editor
      focus: true                  // set focus to editable area after initializing summernote
    });

    $("#InputFiles").fileinput({
    	showUpload: true,
    	'previewFileType' : 'any',
        uploadAsync: false,
        uploadUrl: "/api/addGalleryPhotos",
//        showBrowse: false,
        browseOnZoneClick: true,
        maxFileCount: 15,
        allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg', 'bmp'],

        uploadExtraData: function() {
            return {
                bankID: $("#bankIDGallery").val(),
                sysPack: $("#packageGallery").val(),
            };
        }
    }).on("filepredelete", function(event, data) {
        var abort = true;
        if (confirm("Are you sure you want to delete this image?")) {
            abort = false;
        }
        return abort;
    }).on("filebatchuploadsuccess", function() {
        $( "#addGallery" ).click();
    });
</script>