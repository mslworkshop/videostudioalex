<div class="container-fluid">
	<div class="row">
		<div class="col-lg-10 col-sm-offset-1">
			
			<form role="form"  method="POST" id="editGalleryEelement-{{$data->id}}" name="editGalleryEelement-{{$data->id}}" >

		<!--formenctype="multipart/form-data"-->
<input type="text" hidden="hidden" id="bankID-{{$data->id}}" name="bankIDGallery" value="{{$data->id}}">
<input type="text" hidden="hidden" name="exec" value="edit"></input>
<input type="text" hidden="hidden" id="package" name="package" value="sys.gallery"></input>

				<div class="form-group">
					<label class="label label-default" for="exampleInputEmail">Name</label>
					<input class="form-control" id="exampleInputEmail" type="text" name="title" value="{{$data->title}}" />
				</div>

				<div class="form-group">
					<span class="label label-default" for="DescriptionArea">Description</span>
					<textarea class="form-control" name="description" id="DescriptionArea" style="width:100%; height: 277px;">{{$data->description}}</textarea>
				</div>

				<div class="form-group">
					<span class="label label-default" for="VideoArea">YouTube Video ID</span>
					<input class="form-control" name="video_url" id="VideoArea" value="{{$data->video_url}}" />
				</div>

				<div class="form-group">
					<label class="label label-default" for="InputFiles-{{$data->id}}">Upload photos</label>
            <input id="InputFiles-{{$data->id}}" name="files[]" class="file" type="file" multiple data-preview-file-type="text" data-upload-url="/api/addGalleryPhotos">
         			<p>
						<small>Extensions:</small> .png .gif .jpeg .jpg .bmp
					</p>
				</div>

	<div class="form-group col-lg-12">
				<button type="submit" name="submit" id="applyGallery" class="btn btn-default col-md-12">Apply Gallery</button>
	</div>


			</form>
			<div class="form-group col-md-12">
				<pre id="status_report_edit-{{$data->id}}" class="btn-default" style="visibility: hidden;" onclick="ElementHide(this)" title="Click to hide" data-toggle="tooltip"></pre>
                <p class="text-right">Last update: <cite>{{$data->updated_at}}</cite></p>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
    $('.summernote').summernote({
      height: 200,                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null,             // set maximum height of editor
      focus: true                  // set focus to editable area after initializing summernote
    });

    $("#InputFiles-{{$data->id}}").fileinput({
    	showUpload: true,
    	'previewFileType' : 'any',
        uploadAsync: false,
        uploadUrl: "/api/addGalleryPhotos",
//        showBrowse: false,
        browseOnZoneClick: true,
        maxFileCount: 15,
        allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg', 'bmp'],

        
<?
    $s1 = null;
    $s2 = null;
    $c=1;

    foreach($files as $file)
        {
            $s1 .= "\"<img style='height:160px' src='".App\Http\Controllers\UploadController::retrivePathWithDate($file->pushtime).$file->file_name."'>\",";
            $s2 .= "{caption: \"".$file->file_name."\", size: \"".$file->file_size."\", width: \"120px\", url: \"/api/deletePicture/".$file->id."\", key: $c},";
            $c++;
        }
        echo "initialPreview: [$s1],";
        echo "initialPreviewConfig: [$s2],"
?>
        uploadExtraData: function() {
            return {
                bankID: $("#bankID-{{$data->id}}").val(),
                sysPack: $("#package").val(),
            };
        }
    }).on("filepredelete", function(jqXHR) {
        
        if (confirm("Are you sure you want to delete this image?")) {
            return true;
        }
        event.preventDefault();
    }).on("filebatchuploadsuccess", function() {
        $( "#applyGallery" ).click();
    });

$(function(){
                $("#editGalleryEelement-{{$data->id}}").submit(function(event){
                event.preventDefault();
                var divID = "#status_report_edit-{{$data->id}}";

                $.ajax({
                    method: 'POST',
                    url: '/api/editGallery',
                    dataType: "json",
                   // contentType: "application/json charset=UTF-8",

                      contentType: "application/x-www-form-urlencoded",
                    data : $('#editGalleryEelement-{{$data->id}}').serializeArray() ,
                    success: function(data){
                       //$(divID).attr("title",err.action)
                    if(data.status == "danger"){
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        }else {
                            $(divID).html(data.report);
                        }
                    } else {
                        if(data.debug){
                            $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                        } else {
                            $(divID).html(data.report);
                        }
                    }
                        setClass(divID, data.status)
                        $(divID).visible();
                        $(divID).sf();
                    },
                    error: function(xhr, desc, err){
                    if(err.status == "danger"){
                        $(divID).html(err.report+"::<br>"+err.exec+" ("+data.ID+")");
                    } else {
                        $(divID).html(err.report);
                    }
                        setClass(divID, err.status)
                        $(divID).visible();
                        $(divID).sf();
                    },  complete: function() {
                         setTimeout(function(){ window.location="/gallery" }, 1000);
                    }
                });
            });
});
</script>