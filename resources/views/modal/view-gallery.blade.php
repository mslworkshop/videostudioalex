
@foreach($query as $row)

			<!--- Begin View Window -->
			<div class="modal fade" id="gallery-view-{{$row->data->id}}" role="dialog" aria-labelledby="ModalLabelView" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							 
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								X
							</button>
							<h4 class="modal-title" id="ModalLabelView">
								{{$row->data->title}}
							</h4>
						</div>
						<div class="modal-body">

							@include('gallery.view', ['data' => $row->data, 'files' => $row->files])
						</div>
					</div>
				</div>
			</div>
@endforeach