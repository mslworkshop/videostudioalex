			<!--- Begin Edit window -->
			<div class="modal fade" id="review-edit-{{$data->id}}" role="dialog" aria-labelledby="ModalLabelEdit" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							 
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								X
							</button>
							<h4 class="modal-title" id="ModalLabelEdit">
								{{$data->title}} :: Edit Mode
							</h4>
						</div>
						<div class="modal-body">
							@include('review.edit')
						</div>
					</div>
				</div>
			</div>