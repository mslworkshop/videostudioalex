    <div class="modal fade" id="modal-add-video" role="dialog" aria-labelledby="myModalLabel003" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                             
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                X
                            </button>
                            <h4 class="modal-title" id="myModalLabel003">
                                Add new Video
                            </h4>
                        </div>
                        <div class="modal-body">
                            @include('video.add')
                        </div>
                    </div>
                    
                </div>
                
            </div>