    <div class="modal fade" id="modal-add-review" role="dialog" aria-labelledby="myModalLabel001" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                             
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                X
                            </button>
                            <h4 class="modal-title" id="myModalLabel001">
                                Add new Review
                            </h4>
                        </div>
                        <div class="modal-body">
                            @include('review.add')
                        </div>
                    </div>
                    
                </div>
                
            </div>