			<!--- Begin View Window -->
			<div class="modal fade" id="review-view-{{$data->id}}" role="dialog" aria-labelledby="ModalLabelView" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							 
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								X
							</button>
							<h4 class="modal-title" id="ModalLabelView">
								{{$data->title}}
							</h4>
						</div>
						<div class="modal-body">
							@include('review.view', ['data' => $data, 'file' => $file])
						</div>
					</div>
				</div>
			</div>