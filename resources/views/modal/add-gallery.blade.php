    <div class="modal fade" id="modal-add-gallery" role="dialog" aria-labelledby="myModalLabel002" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                             
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                X
                            </button>
                            <h4 class="modal-title" id="myModalLabel002">
                                Add new gallery
                            </h4>
                        </div>
                        <div class="modal-body">
                            @include('gallery.add')
                        </div>
                    </div>
                    
                </div>
                
            </div>