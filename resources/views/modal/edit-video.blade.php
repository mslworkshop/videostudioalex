			<!--- Begin Edit window -->
			<div class="modal fade" id="video-edit-{{$data->id}}" role="dialog" aria-labelledby="ModalLabelEdit001" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							 
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								X
							</button>
							<h4 class="modal-title" id="ModalLabelEdit001">
								{{$data->title}} :: Edit Mode
							</h4>
						</div>
						<div class="modal-body">
							@include('video.edit')
						</div>
					</div>
				</div>
			</div>