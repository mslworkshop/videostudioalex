@extends('layouts.modal')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">Owner settings</div>
                <div class="panel-body">
                        @include('auth.passwd', array('data'=>$userObj))
                </div>

            </div>
        @if (Session::has('status_report')) 
                <pre class="col-xs-8 col-xs-offset-2 alert-dismissable">
                        <strong>{{ Session::get('status_report') }}</strong>
                </pre>
        @endif
        </div>
    </div>
</div>
@endsection