
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-10 col-sm-offset-1">
			
			<form role="form"  method="POST" id="addReviewElement" name="addReviewElement" >

		<!--formenctype="multipart/form-data"-->
<input type="text" hidden="hidden" name="bankIDReview" id="bankIDReviewAdd" value="<?  echo substr((time()%rand(9000,9999)^0x01f01337), 2, 4); ?>">
<input type="text" hidden="hidden" name="exec" value="create"></input>
<input type="text" hidden="hidden" name="package" id="packageReview" value="sys.review"></input>
<input type="text" hidden="hidden" name="file_id" id="uploadedFileId" value="0"></input>

				<div class="form-group">
					<label class="label label-default" for="exampleInputEmail">Title</label>
					<input class="form-control" id="exampleInputEmail" type="text" name="title" />
				</div>

				<div class="form-group">
					<span class="label label-default" for="DescriptionArea">Description</span>
					<textarea class="form-control" name="description" id="DescriptionArea" style="width: 100%; height: 277px;"></textarea>
				</div>

				<div class="form-group">
					<label class="label label-default" for="InputReviewFiles">Upload Image</label>
            <input id="InputReviewFiles" name="file" class="file" type="file" data-preview-file-type="text">
         			<p>
                        <small>Extensions: .png .gif .jpeg .jpg .bmp</small>
                    </p>
				</div>

	<div class="form-group col-lg-12">
				<button type="submit" name="submit" id="addSubmitReview" class="btn btn-default col-md-12">Add Review</button>
	</div>


			</form>
			<div class="form-group col-md-12">
				<pre id="status_report_addReview" class="btn-default" style="visibility: hidden;" onclick="ElementHide(this)" title="Click to hide" data-toggle="tooltip"></pre>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $("#InputReviewFiles").fileinput({
    	showUpload: true,
    	'previewFileType' : 'any',
        uploadAsync: false,
        uploadUrl: "/api/addReviewPhoto",
//        showBrowse: false,
        browseOnZoneClick: true,
        maxFileCount: 1,
        allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg', 'bmp'],

        uploadExtraData: function() {
            return {
                bankID: $("#bankIDReviewAdd").val(),
                sysPack: $("#packageReview").val(),
            };
        }
    }).on("filebatchuploadsuccess", function(e, data) {
		if (data.response && data.response.id) {
    		$('#uploadedFileId').val(data.response.id);
		}
        // $( "#addSubmitReview" ).click();
    });
</script>