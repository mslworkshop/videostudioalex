
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-10 col-sm-offset-1">
			
			<form role="form"  method="POST" id="EditReviewElement-{{$data->id}}" name="EditReviewElement-{{$data->id}}" >

		<!--formenctype="multipart/form-data"-->
<input type="text" hidden="hidden" name="bankIDReview" id="bankIDReviewEdit" value="{{$data->id}}">
<input type="text" hidden="hidden" name="exec" value="create"></input>
<input type="text" hidden="hidden" package="package" id="packageReview" value="sys.review"></input>

				<div class="form-group">
					<label class="label label-default" for="exampleInputEmail">Title</label>
					<input class="form-control" id="exampleInputEmail" type="text" name="title" value="{{$data->title}}" />
				</div>

				<div class="form-group">
					<span class="label label-default" for="DescriptionArea">Description</span>
					<textarea class="form-control" name="description" id="DescriptionArea" style="width: 100%; height: 277px;">{{$data->description}} </textarea>
				</div>

				<div class="form-group">
					<label class="label label-default" for="InputReviewFiles-{{$data->id}}">Upload Image</label>
            <input id="InputReviewFiles-{{$data->id}}" name="file" class="file" type="file" data-preview-file-type="text" data-upload-url="/api/addReviewPhoto">
         			<p>
                        <small>Extensions: .png .gif .jpeg .jpg .bmp</small>
                    </p>
				</div>

	<div class="form-group col-lg-12">
				<button type="submit" name="submit" id="applyReview" class="btn btn-default col-md-12">Apply Review</button>
	</div>


			</form>
			<div class="form-group col-md-12">
				<pre id="status_report_EditReview-{{$data->id}}" class="btn-default" style="visibility: hidden;" onclick="ElementHide(this)" title="Click to hide" data-toggle="tooltip"></pre>
                <p class="text-right">Last update: <cite>{{$data->updated_at}}</cite></p>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

    $("#InputReviewFiles-{{$data->id}}").fileinput({
    	showUpload: true,
    	'previewFileType' : 'any',
        uploadAsync: false,
        uploadUrl: "/api/addReviewPhoto",
//        showBrowse: false,
        browseOnZoneClick: true,
        maxFileCount: 1,
        allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg', 'bmp'],
        overwriteInitial: false,

        <? if(strlen($file->query->file_name) >= 5) { ?>
        initialPreview: [
            "<img style='height:160px' src='{{$file->photo}}'>",
        ],
        initialPreviewConfig: [
            {caption: "{{$file->query->file_name}}", size: {{$file->query->file_size}}, url: "/api/deletePicture/{{$file->query->id}}", key: 1},
        ],
 <? } ?>
        uploadExtraData: function() {
            return {
                bankID: $("#EditReviewElement-{{$data->id}} #bankIDReviewEdit").val(),
                sysPack: $("#EditReviewElement-{{$data->id}} #packageReview").val(),
            };
        }
    }).on("filepredelete", function(event, data) {
        
        if (confirm("Are you sure you want to delete this image?")) {
            return true
        }
        return;
    }).on("filepreupload", function() {
        $( "#applyReview" ).click();
    });

    $(function(){
        $("#EditReviewElement-{{$data->id}}").submit(function(event){
            event.preventDefault();
            var divID = "#status_report_EditReview-{{$data->id}}";

            $.ajax({
                method: 'POST',
                url: '/api/editReview',
                dataType: "json",
               // contentType: "application/json charset=UTF-8",

                  contentType: "application/x-www-form-urlencoded",
                data : $('#EditReviewElement-{{$data->id}}').serializeArray() ,
                success: function(data){
                   //$(divID).attr("title",err.action)
                if(data.status == "danger"){
                    if(data.debug){
                        $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                    }else {
                        $(divID).html(data.report);
                    }
                } else {
                    if(data.debug){
                        $(divID).html(data.report+"<br>"+data.exec+" ("+data.ID+")");
                    } else {
                        $(divID).html(data.report);
                    }
                }
                    setClass(divID, data.status)
                    $(divID).visible();
                    $(divID).sf();
                },
                error: function(xhr, desc, err){
                if(err.status == "danger"){
                    $(divID).html(err.report+"::<br>"+err.exec+" ("+data.ID+")");
                } else {
                    $(divID).html(err.report);
                }
                    setClass(divID, err.status)
                    $(divID).visible();
                    $(divID).sf();
                },  complete: function() {
                    window.location="/reviews";
                }
            });
        });
    });
</script>