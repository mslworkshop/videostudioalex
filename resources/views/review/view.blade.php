
<div class="container-fixed">
	<div class="row" style="padding-bottom: 3rem;">

 <!-- starting right column -->
		<div class="col-md-7">

			<div class="col-md-12" style="width: 120%;margin: 0;padding: 0;">

			<a href="{{$file->photo}}" data-toggle="lightbox" data-gallery="review-photo-{{$data->id}}">
				<img src="{{$file->photo}}" class="col-md-5 img-thumbnail" height="200" width="200" class="img-fluid" style="margin-right: 1.1rem; margin-bottom: 1.5rem;">
			</a>

			</div>
</div>
 <!-- end right column -->

<div class="col-md-5" style="margin: 0;padding: 0;">
<div class="row">
			<blockquote>
			<p style="font-size: 14px;">
				{!! $data->description !!}
			</p>
			</blockquote>
</div>

</div>
		
	</div>
</div>

