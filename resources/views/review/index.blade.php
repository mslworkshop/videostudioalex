@extends('layouts.app')
@inject('appReviews','App\Reviews')
@inject('appUploads', 'App\Uploads')

@section('content')
<div class="container-fluid">
			<div class="row">
<?	if($query->items() != null) { ?>
@foreach($query as $data)
<? 
//	$rowImage = $appReviews->pictureByID($data->id);
	$rowImage = $appUploads->getPhoto($data->file_id);

?>
<div class="col-sm-3">
	<div class="thumbnail">
		<h4>{{$data->title}}</h4>
 
@if(count($rowImage)!=null)
			<img alt="{{$data->title}}" src="{{$rowImage->photo}}" style="width: 200px; height: 200px; border-radius: 150px;" class="animated zoomIn" />
@else
		<img src="/images/icon/default_lg.png" style="width: 200px; height: 200px;" class="animated zoomIn" /> 
@endif
<div class="caption">

<!-- create control links -->
 					<p class="text-right">
		<a href="#review-view-{{$data->id}}" role="button" data-toggle="modal" class="btn btn-info"><i class="fa fa-eye" title="Preview" data-toggle="tooltip"></i></a>
		<a href="#review-edit-{{$data->id}}" role="button" class="btn btn-primary" data-toggle="modal"><i class="fa fa-pencil-square-o" aria-hidden="true"  title="Edit" data-toggle="tooltip"></i></a>
		<a class="btn btn-danger" href="api/deleteReview/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true" title="Remove" data-toggle="tooltip"></i></a></p>
	<p class="text-left">
	</p>
</div>
</div>
</div>
	
	@include('modal.edit-review', ['data' => $data, 'file' => $rowImage ])
	@include('modal.view-review', ['data' => $data, 'file' => $rowImage ])

@endforeach

<? } else { ?>
	<h3 class='text-danger' style='text-align: center;'>No available reviews.
		<br><br>
	<a href='#modal-add-review' role='button'  data-toggle='modal'><b class='btn btn-info'>Add new One?</b></a></h3>
<? } ?>

</div>
<div class="row">
			<center>
				{!!$query->links()!!}
			</center>
</div>
</div>
@endsection